package noobbot;

import noobbot.io.Log;
import noobbot.io.MessageReader;
import noobbot.io.MessageSender;
import noobbot.logic.Strategy;
import noobbot.logic.World;
import noobbot.model.*;
import noobbot.stats.Statistics;
import noobbot.utils.ThreadUtils;

import java.io.*;
import java.net.Socket;

public abstract class BaseBot implements Runnable {

    protected abstract void joinGame(Bot bot, MessageSender sender);

    protected abstract Strategy createStrategy(World world, MessageSender sender);

    private boolean joinedGame;

    @SuppressWarnings("RedundantCast")
    public void run(String[] args) throws IOException {
        if (args.length < 4) {
            throw new IllegalArgumentException(
                "You need to provide 4 command line parameters:\n<host> <port> <botname> <botkey>");
        }

        Log.log("Available memory: " + Runtime.getRuntime().maxMemory());

        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        Log.log("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);

        final BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        final PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final MessageReader reader = new MessageReader(in);
        final MessageSender sender = new MessageSender(out);
        final Bot bot = new Bot(botName, botKey);

        // create world with all objects
        World world = new World();

        // create an instance of strategy
        Strategy strategy = createStrategy(world, sender);

        // join game
        joinGame(bot, sender);

        // main loop
        boolean gameOver = false;
        boolean ourCarFinished = false;
        MsgWrapper message;

        // time stats for out bot
        Statistics algorithmStats = new Statistics();
        Statistics networkStats = new Statistics();
        while (!gameOver && (message = reader.readMessage()) != null) {
            // Record starting time in ms
            long msgProcessingStartTime = System.currentTimeMillis();

            // we haven't sent anything yet on this tick
            sender.setMessageSent(false);

            // set the current tick, if it's in the message
            world.setGameId(message.gameId);
            world.setCurrentTick(message.gameTick);

            switch (message.msgType) {
                case "carPositions":
                    if (!ourCarFinished) {
                        try {
                            strategy.handleCarPositions((CarPosition[]) message.data);
                        } catch (Exception e) {
                            Log.error("Unexpected exception", e);
                            sender.sendMessageWithTick(new Ping(), world.getCurrentTick());
                        }
                    } else {
                        sender.sendMessageWithTick(new Ping(), world.getCurrentTick());
                    }
                    break;
                case "join":
                    Log.log("Game started");
                    joinedGame = true;
                    break;
                case "joinRace":
                    Log.log("Game started");
                    joinedGame = true;
                    break;
                case "gameInit":
                    // This gets called once for the qualifier, and once for the race
                    world.init((GameInit) message.data);
                    Log.log("World init: " + message.data);
                    break;
                case "gameStart":
                    // This gets called once for the qualifier, and once for the race
                    strategy.startRace();
                    Log.log("Race started: " + world.getCurrentRaceType());
                    break;
                case "gameEnd":
                    // This gets called once for the qualifier, and once for the race
                    strategy.endRace();
                    Log.log("Race ended: " + world.getCurrentRaceType());

                    // We should exit, but only if it's a testing session
                    // If it's an actual qualifier, we should actually proceed and race
                    if (world.getCurrentRaceType() == World.RaceType.TESTING_SESSION) {
                        gameOver = true;
                    }
                    break;
                case "tournamentEnd":
                    Log.log("Game finished");
                    gameOver = true;
                    break;
                case "yourCar":
                    world.setMyCar((YourCar) message.data);
                    break;
                case "crash":
                    strategy.handleCarCrash((Crash) message.data);
                    break;
                case "dnf":
                    strategy.handleCarDnf((Dnf) message.data);
                    break;
                case "spawn":
                    strategy.handleCarSpawn((Spawn) message.data);
                    break;
                case "turboAvailable":
                    strategy.handleTurboAvailable((TurboAvailable) message.data);
                    break;
                case "turboStart":
                    strategy.handleTurboStart((TurboStart) message.data);
                    break;
                case "turboEnd":
                    strategy.handleTurboEnd((TurboEnd) message.data);
                    break;
                case "lapFinished":
                    // this we just ignore for now
                    break;
                case "finish":
                    if (((Finish) message.data).equals(world.getMyCar())) {
                        // organizers are saying that it's not safe to exit yet and we should stay connected until we receive tournamentEnd
                        ourCarFinished = true;
                    }
                    strategy.handleCarFinish((Finish) message.data);
                    break;
                default:
                    Log.error("Received unrecognized command: " + message.msgType + ", " + message.data);
                    break;
            }

            // calculate processing time
            long msgProcessingEndTime = System.currentTimeMillis();
            long msgProcessingTime = msgProcessingEndTime - msgProcessingStartTime;

            // update stats
            algorithmStats.addValue(msgProcessingTime - sender.getTimeSpent());
            networkStats.addValue(sender.getTimeSpent());

            // add warning if it took too long to respond
            // 1 second / 60 ticks per second = 16ms per tick
            if (msgProcessingTime > 15) {
                Log.error(String.format("Delay is >15 ms: algorithm %d ms, network %d ms", (msgProcessingTime - sender.getTimeSpent()), sender.getTimeSpent()));
            }
        }

        // call finish method of a strategy, so it can write charts for example
        strategy.finish();
        Log.log("Finished race: " + world.getMyCar());
        Log.log("Time stats (algorithm): " + algorithmStats);
        Log.log("Time stats (sending messages over network): " + networkStats);

        in.close();
        out.close();
    }

    protected String[] args;

    @Override
    public void run() {
        try {
            run(args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // this method will be called by external threads for multiplayer testing
    // so that the bots can join one after another, in sequence
    public void waitUntilConnected() {
        while (!joinedGame) {
            ThreadUtils.sleep(100);
        }
    }

}
