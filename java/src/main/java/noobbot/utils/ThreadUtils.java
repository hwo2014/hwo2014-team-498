package noobbot.utils;

public class ThreadUtils {

    public static void sleep(long timeMs) {
        try {
            Thread.sleep(timeMs);
        } catch (InterruptedException e) {
            // waiting for the race to be created
        }
    }

}
