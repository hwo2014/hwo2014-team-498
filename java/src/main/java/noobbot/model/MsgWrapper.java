package noobbot.model;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import noobbot.io.Log;
import noobbot.io.MessageUtils;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class MsgWrapper {

    public final String msgType;
    public final Object data;
    public final String gameId;
    public final Integer gameTick;

    public MsgWrapper(String msgType, Object data, String gameId, Integer gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameId = gameId;
        this.gameTick = gameTick;
    }

    public MsgWrapper(Object data, Integer gameTick) {
        String sName = data.getClass().getSimpleName();
        this.msgType = Character.toLowerCase(sName.charAt(0)) + sName.substring(1);
        this.data = data;
        this.gameId = null;
        this.gameTick = gameTick;
    }

    public static class Adapter extends TypeAdapter<MsgWrapper> {

        @SuppressWarnings("unchecked")
        @Override
        public void write(JsonWriter out, MsgWrapper msg) throws IOException {
            out.beginObject();
            out.name("msgType");
            out.value(msg.msgType);

            // write data (unless it's a Ping command)
            if (msg.data != null && !(msg.data instanceof Ping)) {
                out.name("data");
                TypeAdapter adapter = MessageUtils.getAdapter(msg.data.getClass());
                adapter.write(out, msg.data);
            }

            if (msg.gameTick != null) {
                out.name("gameTick");
                out.value(msg.gameTick);
            }
            out.endObject();
        }

        @Override
        public MsgWrapper read(JsonReader in) throws IOException {
            in.beginObject();

            String msgType = null;
            Object data = null;
            String gameId = null;
            Integer gameTick = null;

            // decode everything
            while (in.peek() != JsonToken.END_OBJECT) {
                String name = in.nextName();
                switch (name) {
                    case "msgType":
                        msgType = in.nextString();
                        break;
                    case "data":
                        if (msgType == null) {
                            throw new IllegalStateException("msgType wasn't read before data");
                        }
                        boolean isArray = in.peek() == JsonToken.BEGIN_ARRAY;
                        Class<?> aClass;
                        try {
                            String className = getClass().getPackage().getName()
                                    + "." + Character.toUpperCase(msgType.charAt(0)) + msgType.substring(1);
                            if (isArray) {
                                className = className.substring(0, className.length() - 1);
                            }
                            aClass = getClass().getClassLoader().loadClass(className);
                        } catch (ClassNotFoundException e) {
                            aClass = Object.class;
                        }
                        if (isArray) {
                            in.beginArray();
                            ArrayList<Object> list = new ArrayList<>();
                            while (in.peek() != JsonToken.END_ARRAY) {
                                list.add(MessageUtils.decodeJson(in, aClass));
                            }
                            data = Array.newInstance(aClass, list.size());
                            list.toArray((Object[]) data);
                            in.endArray();
                        } else {
                            data = MessageUtils.decodeJson(in, aClass);
                        }
                        break;
                    case "gameId":
                        gameId = in.nextString();
                        break;
                    case "gameTick":
                        gameTick = Integer.parseInt(in.nextString());
                        break;
                    default:
                        Log.error("Unknown property in input: " + msgType + ", " + name);
                        in.skipValue();
                        break;
                }

            }
            in.endObject();

            return new MsgWrapper(msgType, data, gameId, gameTick);
        }
    }
}

