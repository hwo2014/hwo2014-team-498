package noobbot.model;

public class JoinRace {

    public final Bot botId;
    public final int carCount;
    public final String trackName;
    public final String password;

    public JoinRace(Bot bot, int carCount) {
        this(bot, carCount, null, null);
    }

    public JoinRace(Bot bot, int carCount, String trackName) {
        this(bot, carCount, trackName, null);
    }

    public JoinRace(Bot bot, int carCount, String trackName, String password) {
        this.botId = bot;
        this.carCount = carCount;
        this.trackName = trackName;
        this.password = password;
    }

}
