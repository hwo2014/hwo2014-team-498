package noobbot.model;

public class CarPosition {

    public final Car.Id id;
    public final double angle;
    public final PiecePosition piecePosition;

    public CarPosition(Car.Id id, double angle, PiecePosition piecePosition) {
        this.id = id;
        this.angle = angle;
        this.piecePosition = piecePosition;
    }

    @Override
    public String toString() {
        return "CarPosition{" +
                "id=" + id +
                ", angle=" + angle +
                ", piecePosition=" + piecePosition +
                '}';
    }

    public static class PiecePosition {
        public final int pieceIndex;
        public final double inPieceDistance;
        public final Lane lane;
        public final int lap;

        public PiecePosition(int pieceIndex, double inPieceDistance, Lane lane, int lap) {
            this.pieceIndex = pieceIndex;
            this.inPieceDistance = inPieceDistance;
            this.lane = lane;
            this.lap = lap;
        }

        @Override
        public String toString() {
            return "PiecePosition{" +
                    "pieceIndex=" + pieceIndex +
                    ", inPieceDistance=" + inPieceDistance +
                    ", lane=" + lane +
                    ", lap=" + lap +
                    '}';
        }

        public static class Lane {
            public final int startLaneIndex;
            public final int endLaneIndex;

            public Lane(int startLaneIndex, int endLaneIndex) {
                this.startLaneIndex = startLaneIndex;
                this.endLaneIndex = endLaneIndex;
            }

            @Override
            public String toString() {
                return "Lane{" +
                        "startLaneIndex=" + startLaneIndex +
                        ", endLaneIndex=" + endLaneIndex +
                        '}';
            }
        }
    }
}
