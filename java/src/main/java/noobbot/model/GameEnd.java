package noobbot.model;

import java.util.Arrays;

public class GameEnd {
    public final Results[] results;
    public final BestLap[] bestLaps;

    public GameEnd(Results[] results, BestLap[] bestLaps) {
        this.results = results;
        this.bestLaps = bestLaps;
    }

    @Override
    public String toString() {
        return "GameEnd{" +
                "results=" + Arrays.toString(results) +
                ", bestLaps=" + Arrays.toString(bestLaps) +
                '}';
    }

    public static class Results {
        public final Car.Id car;
        public final Result result;

        public Results(Car.Id car, Result result) {
            this.car = car;
            this.result = result;
        }

        @Override
        public String toString() {
            return "Results{" +
                    "car=" + car +
                    ", result=" + result +
                    '}';
        }

        public static class Result {
            public final Integer laps;
            public final Integer ticks;
            public final Integer millis;

            public Result(Integer laps, Integer ticks, Integer millis) {
                this.laps = laps;
                this.ticks = ticks;
                this.millis = millis;
            }

            @Override
            public String toString() {
                return "Result{" +
                        "laps=" + laps +
                        ", ticks=" + ticks +
                        ", millis=" + millis +
                        '}';
            }
        }
    }

    public static class BestLap {
        public final Car.Id car;
        public final Result result;

        public BestLap(Car.Id car, Result result) {
            this.car = car;
            this.result = result;
        }

        @Override
        public String toString() {
            return "BestLap{" +
                    "car=" + car +
                    ", result=" + result +
                    '}';
        }

        public static class Result {
            public final Integer lap;
            public final Integer ticks;
            public final Integer millis;

            public Result(Integer lap, Integer ticks, Integer millis) {
                this.lap = lap;
                this.ticks = ticks;
                this.millis = millis;
            }

            @Override
            public String toString() {
                return "Result{" +
                        "lap=" + lap +
                        ", ticks=" + ticks +
                        ", millis=" + millis +
                        '}';
            }
        }
    }
}
