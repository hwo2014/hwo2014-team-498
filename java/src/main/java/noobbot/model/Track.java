package noobbot.model;

import com.google.gson.annotations.SerializedName;
import noobbot.io.Log;
import noobbot.logic.ApproximateTrackGeometry;
import noobbot.logic.CarDataUtils;
import noobbot.logic.World;

import java.util.Arrays;

public class Track {

    public final String id;
    public final String name;
    public final Piece[] pieces;
    public final Lane[] lanes;

    // skipped startingPoint
    public Track(String id, String name, Piece[] pieces, Lane[] lanes) {
        this.id = id;
        this.name = name;
        this.pieces = pieces;
        this.lanes = lanes;

        Arrays.sort(this.lanes, (a, b) -> a.index - b.index);
        for (int i = 0; i < this.lanes.length; i++)
            if (this.lanes[i].index != i) {
                throw new IllegalStateException("Lanes are out of order");
            }
    }

    boolean initApproxCompleted = false;

    public void initSwitchLengths(World world) {
        if (initApproxCompleted) {
            return;
        }
        for (int i = 0; i < pieces.length; i++) {
            pieces[i].initLengthArrays(lanes.length);
            if (pieces[i].hasSwitch) {
                for (int startLaneIdx = 0; startLaneIdx < lanes.length; startLaneIdx++)
                    for (int endLaneIdx = 0; endLaneIdx < lanes.length; endLaneIdx++)
                        if (Math.abs(startLaneIdx - endLaneIdx) == 1) {
                            pieces[i].setApproximateLength(startLaneIdx, endLaneIdx, ApproximateTrackGeometry.getApproximatePieceLength(world, i, startLaneIdx, endLaneIdx));
                        }
            }
            for (int laneIdx = 0; laneIdx < lanes.length; laneIdx++) {
                double len = pieces[i].getSegmentLength(getLaneByIndex(laneIdx));
                pieces[i].setApproximateLength(laneIdx, laneIdx, len);
                pieces[i].setCalculatedLength(laneIdx, laneIdx, len);
            }
        }
        initApproxCompleted = true;
    }

    public Piece getPieceByIndex(int pieceIndex) {
        return pieces[pieceIndex];
    }

    public Lane getLaneByIndex(int laneIndex) {
        return lanes[laneIndex];
    }

    public Lane getBaseLane() {
        return getLaneByIndex(this.lanes.length / 2);
    }

    @Override
    public String toString() {
        return "Track{" +
            "id='" + id + '\'' +
            ", name='" + name + '\'' +
            ", pieces=" + Arrays.toString(pieces) +
            ", lanes=" + Arrays.toString(lanes) +
            '}';
    }

    public static class Piece {
        @SerializedName("switch")
        public final boolean hasSwitch;
        public final Double length;
        public final Double radius;
        public final Double angle;

        // lane1 x lane2
        private double[][] approximateLength;

        // lane1 x lane2
        private double[][] calculatedLength;

        public Piece(boolean hasSwitch, Double length, Double radius, Double angle) {
            this.hasSwitch = hasSwitch;
            this.length = length;
            this.radius = radius;
            this.angle = angle;
        }

        public double getSegmentLength(Lane lane) {
            if (length != null) {
                return length;
            }
            return getLaneRadius(lane) * (Math.abs(angle) * Math.PI / 180.0);
        }

        public double getLaneRadius(Lane lane) {
            if (radius != null) {
                return radius - Math.signum(angle) * lane.distanceFromCenter;
            }
            return Double.POSITIVE_INFINITY;
        }

        @Override
        public String toString() {
            return "Piece{" +
                "hasSwitch=" + hasSwitch +
                ", length=" + length +
                ", radius=" + radius +
                ", angle=" + angle +
                '}';
        }


        public void initLengthArrays(int lanesCnt) {
            approximateLength = new double[lanesCnt][lanesCnt];
            for (double[] v : approximateLength) {
                Arrays.fill(v, Double.NaN);
            }

            calculatedLength = new double[lanesCnt][lanesCnt];
            for (double[] v : calculatedLength) {
                Arrays.fill(v, Double.NaN);
            }
        }

        public void setApproximateLength(int startLaneIdx, int endLaneIdx, double length) {
            approximateLength[startLaneIdx][endLaneIdx] = length;
        }

        public void setCalculatedLength(int startLaneIdx, int endLaneIdx, double length) {
            calculatedLength[startLaneIdx][endLaneIdx] = length;
        }

        // this will always return a good value
        public double getApproximateLength(int startLaneIdx, int endLaneIdx) {
            // approximate length should always be calculated!
            if (Double.isNaN(approximateLength[startLaneIdx][endLaneIdx])) {
                throw new IllegalStateException("Requesting piece length, but passed incorrect lanes: " + startLaneIdx + " -> " + endLaneIdx);
            }
            return approximateLength[startLaneIdx][endLaneIdx];
        }

        // this will either return a calculated value, or -1 if it has not been calculated
        public double getCalculatedLength(int startLaneIdx, int endLaneIdx) {
            // approximate length should always be calculated! (yes, the following line is correct)
            if (Double.isNaN(approximateLength[startLaneIdx][endLaneIdx])) {
                throw new IllegalStateException("Requesting piece length, but passed incorrect lanes: " + startLaneIdx + " -> " + endLaneIdx);
            }
            return calculatedLength[startLaneIdx][endLaneIdx];
        }

        // returns length of the piece
        public double getLength(int startLaneIdx, int endLaneIdx, boolean approximationAllowed) {
            double calculated = getCalculatedLength(startLaneIdx, endLaneIdx);
            if (!Double.isNaN(calculated)) {
                return calculated;
            }

            if (approximationAllowed) {
                double approx = getApproximateLength(startLaneIdx, endLaneIdx);
                if (!Double.isNaN(approx)) {
                    return approx;
                }
                throw new IllegalStateException("Approximate length should always be available");
            }

            return Double.NaN;
        }

        public boolean isSimilar(Piece that) {
            // both straight
            boolean eqStraight = (this.length != null && that.length != null && Math.abs(this.length - that.length) < CarDataUtils.EPS);

            // both curves
            boolean eqCurve = (this.radius != null && that.radius != null && Math.abs(this.radius - that.radius) < CarDataUtils.EPS) &&
                              (this.angle != null && that.angle != null && Math.abs(this.angle - that.angle) < CarDataUtils.EPS);

            return eqStraight || eqCurve;
        }

        public void foundSwitchLength(World world, int startLaneIdx, int endLaneIdx, double goodValue, String message) {
            if (!hasSwitch) {
                throw new IllegalStateException("There must be a switch here");
            }

            // if we calculated too low, then just ignore it
            double approxValue = getApproximateLength(startLaneIdx, endLaneIdx);
            if (goodValue < approxValue * 0.95 || goodValue > approxValue * 1.05) {
                Log.important("Calculated switch length is too low. Ignoring. " + message);
                return;
            }

            // get approximate value
            int laneCnt = world.getRace().track.lanes.length;
            double value = getLength(startLaneIdx, endLaneIdx, true);
            if (Math.abs(goodValue - value) > CarDataUtils.EPS) {
                Log.important("Refining switch length and similar pieces. " + message);

                // update all similar switches
                if (this.length != null) {
                    // update similar straight pieces, including ours
                    for (Piece that : world.getRace().track.pieces)
                        if (that.hasSwitch && isSimilar(that)) {
                            // update all switches within a piece at once
                            for (int i = 0; i < laneCnt; i++)
                                for (int j = 0; j < laneCnt; j++)
                                    if (Math.abs(i - j) == 1) {
                                        that.calculatedLength[i][j] = goodValue;
                                    }
                        }
                } else {
                    // update similar curved pieces, including ours
                    for (Piece that : world.getRace().track.pieces)
                        if (that.hasSwitch && isSimilar(that)) {
                            // update just once switch within a piece
                            that.calculatedLength[startLaneIdx][endLaneIdx] = goodValue;
                        }
                }

            }

        }

    }

    public static class Lane {
        public final double distanceFromCenter;
        public final int index;

        public Lane(double distanceFromCenter, int index) {
            this.distanceFromCenter = distanceFromCenter;
            this.index = index;
        }

        @Override
        public String toString() {
            return "Lane{" +
                "distanceFromCenter=" + distanceFromCenter +
                ", index=" + index +
                '}';
        }
    }
}
