package noobbot.model;

public class Dnf {

    public final Car.Id car;
    public final String reason;

    public Dnf(Car.Id car, String reason) {
        this.car = car;
        this.reason = reason;
    }

    public Car.Id getCar() {
        return car;
    }

    public String getReason() {
        return reason;
    }

}
