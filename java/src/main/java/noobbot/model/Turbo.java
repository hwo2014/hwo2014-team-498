package noobbot.model;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class Turbo {

    private String message;

    public Turbo(String message) {
        this.message = message;
    }

    public static class Adapter extends TypeAdapter<Turbo> {

        @Override
        public void write(JsonWriter out, Turbo value) throws IOException {
            out.value(value.message);
        }

        @Override
        public Turbo read(JsonReader in) throws IOException {
            return new Turbo(in.nextString());
        }
    }

}
