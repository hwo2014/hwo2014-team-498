package noobbot.model;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class SwitchLane {
    String direction;

    public SwitchLane(int laneFrom, int laneTo) {
        if (laneTo < laneFrom) {
            direction = "Left";
        } else if (laneTo > laneFrom) {
            direction = "Right";
        } else {
            throw new IllegalStateException("Not a lane switch");
        }
    }

    public SwitchLane(String direction) {
        this.direction = direction;
    }

    public static class Adapter extends TypeAdapter<SwitchLane> {

        @Override
        public void write(JsonWriter out, SwitchLane value) throws IOException {
            out.value(value.direction);
        }

        @Override
        public SwitchLane read(JsonReader in) throws IOException {
            return new SwitchLane(in.nextString());
        }
    }
}
