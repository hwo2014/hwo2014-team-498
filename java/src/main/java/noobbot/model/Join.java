package noobbot.model;

public class Join {

    public final String name;
    public final String key;

    public Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    public Join(Bot bot) {
        this.name = bot.getName();
        this.key = bot.getKey();
    }

}
