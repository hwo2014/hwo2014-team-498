package noobbot.model;

public class Bot {

    public final String name;
    public final String key;

    public Bot(String name, String key) {
        this.name = name;
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

}
