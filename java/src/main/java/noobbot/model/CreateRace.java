package noobbot.model;

public class CreateRace extends JoinRace {

    public CreateRace(Bot bot, int carCount) {
        super(bot, carCount);
    }

    public CreateRace(Bot bot, int carCount, String trackName) {
        super(bot, carCount, trackName);
    }

    public CreateRace(Bot bot, int carCount, String trackName, String password) {
        super(bot, carCount, trackName, password);
    }

}
