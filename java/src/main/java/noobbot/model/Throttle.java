package noobbot.model;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class Throttle {
    double value;

    public Throttle(double value) {
        this.value = value;
    }

    public static class Adapter extends TypeAdapter<Throttle> {

        @Override
        public void write(JsonWriter out, Throttle value) throws IOException {
            out.value(value.value);
        }

        @Override
        public Throttle read(JsonReader in) throws IOException {
            return new Throttle(in.nextDouble());
        }
    }

}
