package noobbot.model;

public class GameInit {

    public final Race race;

    GameInit(Race race) {
        this.race = race;
    }

    @Override
    public String toString() {
        return race.toString();
    }

}
