package noobbot.model;

public class TurboAvailable {

    private final double turboDurationMilliseconds;
    private final double turboFactor;
    private final int turboDurationTicks;

    public TurboAvailable(double turboFactor, int turboDurationTicks, double turboDurationMilliseconds) {
        this.turboFactor = turboFactor;
        this.turboDurationTicks = turboDurationTicks;
        this.turboDurationMilliseconds = turboDurationMilliseconds;
    }

    public int getTurboDurationTicks() {
        if (this.turboDurationTicks == 0) {
            return (int) Math.round(turboDurationMilliseconds * 60 / 1000.0);
        }
        return turboDurationTicks;
    }

    public double getTurboDurationMilliseconds() {
        return turboDurationMilliseconds;
    }

    public double getTurboFactor() {
        return turboFactor;
    }

    @Override
    public String toString() {
        return "TurboAvailable{" +
            "ticks=" + getTurboDurationTicks() +
            ", factor=" + getTurboFactor() +
            '}';
    }

}
