package noobbot.model;

public class Car {
    public final Id id;
    public final Dimensions dimensions;

    public Car(Id id, Dimensions dimensions) {
        this.id = id;
        this.dimensions = dimensions;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", dimensions=" + dimensions +
                '}';
    }

    public static class Id {
        public final String name;
        public final String color;

        public Id(String name, String color) {
            this.name = name;
            this.color = color;
        }

        public Id(Id id) {
            this(id.name, id.color);
        }

        @Override
        public String toString() {
            return "Car.Id{" +
                    "name='" + name + '\'' +
                    ", color='" + color + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null) return false;

            Id id = (Id) o;
            return color.equals(id.color) && name.equals(id.name);
        }

        @Override
        public int hashCode() {
            int result = name.hashCode();
            result = 31 * result + color.hashCode();
            return result;
        }
    }

    public static class Dimensions {
        public final double length;
        public final double width;
        public final double guideFlagPosition;

        public Dimensions(double length, double width, double guideFlagPosition) {
            this.length = length;
            this.width = width;
            this.guideFlagPosition = guideFlagPosition;
        }

        @Override
        public String toString() {
            return "Dimensions{" +
                    "length=" + length +
                    ", width=" + width +
                    ", guideFlagPosition=" + guideFlagPosition +
                    '}';
        }
    }

}
