package noobbot.model;

public class Race {
    public final Track track;
    public final Car[] cars;
    public final RaceSession raceSession;

    public Race(Track track, Car[] cars, RaceSession raceSession) {
        this.track = track;
        this.cars = cars;
        this.raceSession = raceSession;
    }

    @Override
    public String toString() {
        return "Race {" +
                track.id +
                ", pieces=" + track.pieces.length +
                ", lanes=" + track.lanes.length +
                ", cars=" + cars.length +
                ", laps=" + raceSession.laps +
                "}";
    }

    public static class RaceSession {
        public final int laps;
        public final long maxLapTimeMs;
        public final boolean quickRace;
        public final int durationMs;

        public RaceSession(int laps, long maxLapTimeMs, boolean quickRace, int durationMs) {
            this.laps = laps;
            this.maxLapTimeMs = maxLapTimeMs;
            this.quickRace = quickRace;
            this.durationMs = durationMs;
        }

        @Override
        public String toString() {
            return "RaceSession{" +
                    "laps=" + laps +
                    ", maxLapTimeMs=" + maxLapTimeMs +
                    ", quickRace=" + quickRace +
                    ", durationMs=" + durationMs +
                    '}';
        }
    }
}
