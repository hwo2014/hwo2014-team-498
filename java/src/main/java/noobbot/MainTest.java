package noobbot;

import noobbot.io.MessageSender;
import noobbot.logic.PhysicsStrategy;
import noobbot.logic.Strategy;
import noobbot.logic.World;
import noobbot.model.Bot;
import noobbot.model.JoinRace;

import java.io.IOException;

/**
 * This is class that we can use during the testing of our Bot.
 *
 * Here you can:
 *   - experiment with different strategies
 *   - enable generation of charts
 *   - enable verbose logging
 *
 * Example of parameters:
 *   testserver.helloworldopen.com 8091 "Team Saratov" 3Ad3PGsKxyVI5A
 */
public class MainTest extends BaseBot {

    // select a track here
    private static final Track TRACK = Track.FRANCE;

    public static void main(String... args) throws IOException {
        new MainTest().run(args);
    }

    @Override
    public Strategy createStrategy(World world, MessageSender sender) {
//        Strategy strategy = new TrackLearningStrategy(world, sender);
        Strategy strategy = new PhysicsStrategy(world, sender);
        strategy.enableCharts();
        return strategy;
    }

    @Override
    protected void joinGame(Bot bot, MessageSender sender) {
        JoinRace join = new JoinRace(bot, 1, TRACK.getName());
        sender.sendMessageNoTick(join);
    }

}