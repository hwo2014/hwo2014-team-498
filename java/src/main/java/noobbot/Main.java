package noobbot;

import noobbot.io.MessageSender;
import noobbot.logic.PhysicsStrategy;
import noobbot.logic.Strategy;
import noobbot.logic.World;
import noobbot.model.Bot;
import noobbot.model.Join;

import java.io.IOException;

/**
 *
 * This is the main class of our Bot.
 *
 * It participates in CI and automated tests on the server, so:
 *   - it should always use a good strategy
 *   - it should use 'join' command to join the race
 *   - it should not write any charts or logs
 *
 *   **********************************
 *   BE CAREFUL WHEN CHANGING THIS CODE
 *   **********************************
 */
public class Main extends BaseBot {

    public static void main(String... args) throws IOException {
        new Main().run(args);
    }

    @Override
    public Strategy createStrategy(World world, MessageSender sender) {
        return new PhysicsStrategy(world, sender);
    }

    @Override
    protected void joinGame(Bot bot, MessageSender sender) {
        Join join = new Join(bot);
        sender.sendMessageNoTick(join);
    }

}

