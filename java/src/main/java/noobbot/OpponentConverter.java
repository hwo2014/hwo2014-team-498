package noobbot;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import noobbot.charts.ChartRenderer;
import noobbot.io.Log;
import noobbot.io.MessageUtils;
import noobbot.logic.CarDataUtils;
import noobbot.logic.World;
import noobbot.model.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Converter for opponent race data.
 * <p/>
 * Usage example: java -cp target/noobbot-0.1-SNAPSHOT-jar-with-dependencies.jar noobbot.OpponentConverter https://hwo2014-racedata-prod.s3.amazonaws.com/test-races/97f5c7d1-c75d-4eba-bba1-35bebf7b7a19.json 0
 * Usage example: java -cp target/noobbot-0.1-SNAPSHOT-jar-with-dependencies.jar noobbot.OpponentConverter charts/opponent/ours-unprocessed.js 0
 */
public class OpponentConverter {
    private static final World world = new World();

    private static final ChartRenderer chartRenderer = new ChartRenderer();
    public static int CAR_NUMBER = 0;

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            Log.error("USAGE: OpponentConverter <RACE_LINK> <CAR_NUMBER>");
            return;
        }

        Log.log("Fetching " + args[0]);
        final InputStream jsonData;
        if (args[0].startsWith("http")) {
            if (args[0].startsWith("https://helloworldopen.com/race/")) {
                InputStream data = null;
                try (BufferedReader reader = new BufferedReader(new InputStreamReader((new URL(args[0]).openStream())))) {
                    for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                        if (line.contains("https://hwo2014-racedata-prod.s3.amazonaws.com/test-races/")) {
                            int p = line.indexOf("https://hwo2014-racedata-prod.s3.amazonaws.com/test-races/");
                            String url = line.substring(p, line.indexOf(".json\">") + 5);
                            data = new GZIPInputStream(new URL(url).openStream());
                        }
                    }
                }
                if (data == null) {
                    throw new IllegalArgumentException("Wrong link");
                } else {
                    jsonData = data;
                }
            } else {
                jsonData = new GZIPInputStream(new URL(args[0]).openStream());
            }
        } else {
            jsonData = new FileInputStream(args[0]);
        }
        CAR_NUMBER = Integer.parseInt(args[1]);
        try {
            processStream(jsonData);
        } finally {
            jsonData.close();
        }
        chartRenderer.setOpponent(true);
        chartRenderer.setTurbo(turbos);
        chartRenderer.writeTo(String.format("charts/data-opponent-%s.js", world.getRace().track.name.toLowerCase()), opName, world.getRace().track.name.toLowerCase());
    }

    private static void processStream(InputStream jsonData) {
        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse(new InputStreamReader(jsonData)).getAsJsonArray();
        for (int i = 0; i < array.size(); i++) {
            feed(array.get(i));
        }
    }

    static int lap = 0;
    static double position = 0;
    static double angle = 0;
    static double radius = 0;
    static boolean started = false;
    static Map<Integer, List<double[]>> turbos = new HashMap<>();
    static double turboStart = 0;
    static int turboStartLap = -2;
    static String opName;

    @SuppressWarnings("unchecked")
    private static void feed(JsonElement e) {
        MsgWrapper msg = MessageUtils.decodeJson(e, MsgWrapper.class);
        switch (msg.msgType) {
            case "gameInit": {
                world.init((GameInit) msg.data);
                break;
            }
            case "fullCarPositions": {
                if (!started) {
                    break;
                }
                Map<String, Object> data = (Map<String, Object>) (((Object[]) msg.data)[CAR_NUMBER]);
                Map<String, Object> piecePositionData = (Map<String, Object>) data.get("piecePosition");
                lap = ((Double) piecePositionData.get("lap")).intValue();
                int pieceIndex = ((Double) piecePositionData.get("pieceIndex")).intValue();
                double inPieceDistance = (Double) piecePositionData.get("inPieceDistance");
                Car.Id id = new Car.Id((String) ((Map<String, Object>) data.get("id")).get("name"), (String) ((Map<String, Object>) data.get("id")).get("color"));
                opName = id.name;
                CarPosition.PiecePosition.Lane lane = new CarPosition.PiecePosition.Lane(((Double) ((Map<String, Object>) piecePositionData.get("lane")).get("startLaneIndex")).intValue(),
                        ((Double) ((Map<String, Object>) piecePositionData.get("lane")).get("endLaneIndex")).intValue());
                CarPosition.PiecePosition piecePosition = new CarPosition.PiecePosition(pieceIndex, inPieceDistance, lane, lap);
                angle = (Double) data.get("angleOffset");
                CarPosition pos = new CarPosition(id, angle, piecePosition);

                position = CarDataUtils.getCarPositionInBaseLane(world, pos, true);
                noobbot.model.Track.Piece piece = world.getRace().track.pieces[piecePosition.pieceIndex];
                radius = piece.radius == null ? 0 : piece.radius;
                break;
            }
            case "carVelocities": {
                if (!started) {
                    break;
                }
                Map<String, Object> data = (Map<String, Object>) (((Object[]) msg.data)[CAR_NUMBER]);
                double speed = Math.sqrt((Double) data.get("x") * (Double) data.get("x") + (Double) data.get("y") * (Double) data.get("y"));
                if (speed > CarDataUtils.EPS) {
                    chartRenderer.addData(lap, position, speed, angle, radius == Double.POSITIVE_INFINITY ? 0 : radius, 0.0);
                }
                break;
            }
            case "gameStart": {
                started = true;
                break;
            }
            case "turboStart": {
                TurboStart data = (TurboStart) msg.data;
                Car.Id carId = new Car.Id(data.name, data.color);
                if (world.getCarID(carId) != CAR_NUMBER) {
                    break;
                }
                turboStart = position;
                turboStartLap = lap;
                break;
            }
            case "turboEnd": {
                TurboEnd data = (TurboEnd) msg.data;
                Car.Id carId = new Car.Id(data.name, data.color);
                if (world.getCarID(carId) != CAR_NUMBER) {
                    break;
                }
                while (turboStartLap != lap) {
                    putTurboRecord(turboStartLap, turboStart, CarDataUtils.getTotalTrackLengthInBaseLane(world));
                    turboStartLap++;
                    turboStart = 0;
                }
                putTurboRecord(turboStartLap, turboStart, position);
                break;
            }
        }
    }

    private static void putTurboRecord(int lap, double turboStart, double turboEnd) {
        List<double[]> t = turbos.get(lap);
        if (t == null) {
            t = new ArrayList<>();
            turbos.put(lap, t);
        }
        t.add(new double[]{turboStart, turboEnd});
    }
}
