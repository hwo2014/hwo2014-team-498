package noobbot;

import noobbot.io.MessageSender;
import noobbot.logic.PhysicsStrategy;
import noobbot.logic.Strategy;
import noobbot.logic.World;
import noobbot.model.Bot;
import noobbot.model.JoinRace;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * This is class that we can use during the testing of our Bot on all available tracks.
 * <p/>
 * Here you can:
 * - experiment with different strategies
 * - enable generation of charts
 * - enable verbose logging
 * <p/>
 * Example of parameters:
 * testserver.helloworldopen.com 8091 "Team Saratov" 3Ad3PGsKxyVI5A
 */
public class AllTracksTest extends BaseBot {

    // select a track here
    private final Track track;

    public AllTracksTest(Track track) {
        this.track = track;
    }

    public static void main(String... args) throws IOException {
        for (Track track : Track.values()) {
            String threadName = String.format("track-%s", track.getName().toLowerCase());
            new Thread(() -> {
                try {
                    new AllTracksTest(track).run(args);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }, threadName).start(); // No need to join non-daemon threads - JVM will exit only when all non-daemon threads exit
        }
   }

    @Override
    public Strategy createStrategy(World world, MessageSender sender) {
        Strategy strategy = new PhysicsStrategy(world, sender, track.getName());
        strategy.enableCharts();
        return strategy;
    }

    @Override
    protected void joinGame(Bot bot, MessageSender sender) {
        JoinRace join = new JoinRace(bot, 1, track.getName());
        sender.sendMessageNoTick(join);
    }

}