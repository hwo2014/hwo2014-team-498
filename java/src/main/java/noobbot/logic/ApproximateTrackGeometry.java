package noobbot.logic;

import noobbot.model.Track;

public class ApproximateTrackGeometry {

    // do not use this method, unless you know what you are doing
    // it just returns the approximate value, which is often not acceptable for us
    @SuppressWarnings("UnnecessaryLocalVariable")
    public static double getApproximatePieceLength(World world, int pieceIdx, int laneStartIdx, int laneEndIdx) {
        Track.Piece piece = world.getRace().track.pieces[pieceIdx];
        Track.Lane lane1 = world.getRace().track.getLaneByIndex(laneStartIdx);
        Track.Lane lane2 = world.getRace().track.getLaneByIndex(laneEndIdx);

        if (piece.angle == null) {
            // straight line, may be with a switch
            double h = piece.length;
            double w = Math.abs(lane1.distanceFromCenter - lane2.distanceFromCenter);
            return Math.sqrt(1.0002 * h * h + 1.0357 * w * w);
        } else {
            // arc, may be with a switch
            double r1 = piece.getLaneRadius(lane1);
            double r2 = piece.getLaneRadius(lane2);
            double angle = Math.abs(piece.angle) * Math.PI / 180.0;
            double a = (r2 - r1) / angle;
            double b = r1;
            return switchIntegralValue(a, b, angle) - switchIntegralValue(a, b, 0);
        }
    }

    private static double switchIntegralValue(double a, double b, double phi) {
        double apb = a * phi + b;
        double sqrt = Math.sqrt(a * a + apb * apb);
        double p1 = 0.5 * sqrt * apb / a;
        double p2 = 0.5 * a * Math.log(sqrt + apb);
        return p1 + p2;
    }

}
