package noobbot.logic;

import noobbot.io.Log;
import noobbot.io.MessageSender;
import noobbot.logic.physics.CarPhysics;
import noobbot.logic.physics.Prediction;
import noobbot.logic.physics.WorldPhysics;
import noobbot.model.*;

import java.io.*;
import java.util.ArrayList;

public class PhysicsStrategy extends Strategy {

    private static final boolean ALLOW_APPROXIMATE_VALUES = true;

    private static final boolean DUMP_SAMPLES = false;

    WorldPhysics worldPhysics;
    CarPhysics[] carPhysics;
    CarPhysics myCar;
    RacingLine racingLine;

    TurboManager turboManager;
    int cleanLap = 1;
    double cleanLapPosition = 0;
    double lapMaxAngle = 0;

    public PhysicsStrategy(World world, MessageSender sender) {
        this(world, sender, null);
    }

    public PhysicsStrategy(World world, MessageSender sender, String threadSuffix) {
        super(world, sender);
        worldPhysics = new WorldPhysics(world, threadSuffix);
        world.setPhysics(worldPhysics);
        init();
    }

    private void init() {
        turboManager = null;
        cleanLap = 1;
        cleanLapPosition = 0;
        lapMaxAngle = 0;
    }

    @Override
    public Move processCarPositions() {
        CarData myCarData = getWorld().getMyCarData();

        int lap = myCarData.getPos().piecePosition.lap;
        double posInBaseLane = myCarData.getPosInBaseLane(ALLOW_APPROXIMATE_VALUES);
        lapMaxAngle = Math.max(lapMaxAngle, myCarData.getAngle());
        if (lap > cleanLap || lap == cleanLap && posInBaseLane > cleanLapPosition) {
            worldPhysics.setCriticalAngle(55 * worldPhysics.getCriticalAngle() / lapMaxAngle); // TODO: constant
            cleanLap = myCarData.getPos().piecePosition.lap + 1;
            cleanLapPosition = myCarData.getPosInBaseLane(ALLOW_APPROXIMATE_VALUES);
            lapMaxAngle = 0;
        }

        if (carPhysics == null) {
            carPhysics = new CarPhysics[world.getRace().cars.length];
            for (int i = 0; i < carPhysics.length; i++) {
                carPhysics[world.getCarID(world.getRace().cars[i].id)] = new CarPhysics(world, worldPhysics, world.getRace().cars[i].id);
            }
            myCar = carPhysics[world.getMyCarID()];
        }

        for (Car car : world.getRace().cars) {
            carPhysics[world.getCarID(car.id)].updateCarPosition();
        }

        if (!worldPhysics.isTrustable() && Math.abs(myCarData.getAngle()) < 10) {
            // if we're still learning track params, allow to raise speed when our angle is small
            CarPosition.PiecePosition piecePosition = myCarData.getPos().piecePosition;
            double mySpeed = myCarData.getSpeed(false);
            if (Double.isFinite(mySpeed) && Math.abs(mySpeed - myCarData.getPrevSpeed(true)) < 1) {
                double speed = worldPhysics.getSpeedAfterThrottle(
                        mySpeed, myCarData.getLastThrottleSent(), 2);
                double radius = CarDataUtils.getActualPieceRadius(world, piecePosition.pieceIndex,
                        piecePosition.lane.startLaneIndex, piecePosition.lane.endLaneIndex);
                worldPhysics.setV2rSlipStartForLearning(speed * speed / Math.sqrt(Math.abs(radius)));
            }
        }

        int pieceIndex = myCarData.getPos().piecePosition.pieceIndex;
        int trackPieceCount = getWorld().getRace().track.pieces.length;

        // call this just for the point validating the current position
        CarDataUtils.getActualDistanceToPieceEnd(getWorld(), myCarData.getPos(), false);

        // calculate racing line
        if (racingLine == null) {
            racingLine = new ShortestRacingLine(getWorld());
        }

        if (turboManager == null) {
            turboManager = new TurboManager(world, worldPhysics, racingLine, this);
        }

        int[] lanes = racingLine.getNextLanes(myCarData.getPos(), true);
        int optimalLaneNextSwitch = racingLine.getLaneAfterNextSwitch(myCarData.getPos(), lanes);

        double mySpeed = myCarData.getSpeed(false);
        boolean cantCalculateSpeed = false;
        if (Double.isNaN(mySpeed)) {
            // if out speed is unreliable, trying to calculate it based on the previous speed and throttle
            double prevSpeed = myCarData.getPrevSpeed(false);
            if (Double.isNaN(prevSpeed)) {
                mySpeed = myCarData.getSpeed(true); // approximate
                cantCalculateSpeed = true;
            } else {
                mySpeed = worldPhysics.getSpeedAfterThrottle(
                        prevSpeed, myCarData.getLastThrottleSent() * myCarData.getPrevTurboFactor(), 1);
            }
        }
        double throttle;

        // if mySpeed == 0, we crashed, safe to return 1.0
        if (mySpeed != 0) {
            ArrayList<Double> pPieceLength = new ArrayList<>();
            ArrayList<RadiusProvider> pPieceRadii = new ArrayList<>();

            int offset = 0;

            // while (length < lookaheadDistance && offset < world.getRace().track.pieces.length...

            // TODO: logic for checking last lap should be different in qualification
            boolean isQual = world.getRace().raceSession.laps == 0;
            while (offset < world.getRace().track.pieces.length && offset + 1 < lanes.length
                    && (isQual || lap + (pieceIndex + offset) / trackPieceCount < world.getRace().raceSession.laps)) {

                int offsetPieceIdx = (pieceIndex + offset) % trackPieceCount;
                Track.Piece piece = getWorld().getRace().track.getPieceByIndex(offsetPieceIdx);

                double pieceLen = piece.getLength(lanes[offset], lanes[offset + 1], ALLOW_APPROXIMATE_VALUES);
                pPieceLength.add(pieceLen);

                pPieceRadii.add(worldPhysics.getRadiusProvider(piece, lanes[offset], lanes[offset + 1]));

                offset++;
            }

            double[] segmentLengths = new double[pPieceLength.size()];
            RadiusProvider[] segmentRadii = new RadiusProvider[pPieceLength.size()];
            for (int i = 0; i < segmentLengths.length; i++) {
                segmentLengths[i] = pPieceLength.get(i);
                segmentRadii[i] = pPieceRadii.get(i);
            }

            double startAt = myCarData.getPos().piecePosition.inPieceDistance;
            double myAngle = myCarData.getAngle();
            double myAngleSpeed = myCarData.getAngleSpeed();
            Prediction prediction = worldPhysics.getPrediction(
                    segmentLengths, segmentRadii, startAt, lap, mySpeed, myAngle, myAngleSpeed);
            throttle = getBestThrottle(prediction, myCarData.getTurboFactor(), true);
            if (throttle > 0 && !worldPhysics.isTrustable()) {
                // more aggressive learning
                throttle = 1.0;
            }
        } else {
            throttle = 1.0;
        }

        // we're braking here because we don't know our real speed.
        if (cantCalculateSpeed) {
            throttle = 0.0;
        }

        if (throttle > 1.0) {
            throttle = 1.0;
        } else if (throttle < 0.0) {
            throttle = 0.0;
        }

        Move move = new Move();
        move.setLaneChange(optimalLaneNextSwitch);
        move.setThrottle(throttle);
        if (turboManager.isGoodTimeForTurbo()) {
            move.setTurboEnabled();
        }
        return move;
    }

    private double getBestThrottle(Prediction prediction, double turboFactor, boolean tryBest) {
        double throttle = 0.0;
        Prediction bestPrediction = null;
        for (double thr : new double[]{1.0, 0.5, 0.0}) {
            // TODO: take into account turbo duration
            Prediction nextPrediction = prediction.move(thr * turboFactor);
            double maxAbsAngle = findMaxAbsAngle(nextPrediction);
            if (maxAbsAngle < worldPhysics.getCriticalAngle()) {
                if (tryBest) {
                    Prediction prediction100 = getBestPrediction(nextPrediction, turboFactor, 100);
                    if (bestPrediction == null || prediction100.getSegmentIndex() > bestPrediction.getSegmentIndex()
                            || prediction100.getSegmentIndex() == bestPrediction.getSegmentIndex()
                            && prediction100.getSegmentPos() > bestPrediction.getSegmentPos()) {
//                        if (bestPrediction != null) {
//                            Log.error("We chose to decrease speed since it'll be faster");
//                        }
                        bestPrediction = prediction100;
                        throttle = thr;
                    }
                } else {
                    // don't try our best, just use highest possible speed
                    throttle = thr;
                    break;
                }
            }
            if (tryBest && thr == 0.0 && bestPrediction == null && maxAbsAngle >= worldPhysics.getCriticalAngle()) {
                Log.error("Tick " + world.getCurrentTick()
                        + ", predicted max angle: " + maxAbsAngle + ". EMERGENCY BRAKING!");
            }
        }
        return throttle;
    }

    private Prediction getBestPrediction(Prediction prediction, double turboFactor, int afterTicks) {
        Prediction result = prediction;
        for (int i = 0; i < afterTicks; i++) {
            double bestThrottle = getBestThrottle(result, turboFactor, false);
            result = result.move(bestThrottle);
        }
        return result;
    }


    public double findMaxAbsAngle(Prediction prediction) {
        double maxAngle = 0;

        int t = 0;
        // TODO: magic constant, use noSlipSpeed
        while (prediction.getSpeed() > 1 && !prediction.hasReachedEnd()) {
            maxAngle = Math.max(maxAngle, Math.abs(prediction.getAngle()));
            prediction = movePrediction(prediction, t);
            t++;
        }
        return maxAngle;
    }

    private Prediction movePrediction(Prediction prediction, int t) {
        final double throttle;
        // if we're learning, think that we're not going to brake next 20 ticks
        if (prediction.hasReachedEnd() || prediction.isRadiusKnown() && worldPhysics.isTrustable() || t > 20) {
            throttle = 0.0;
        } else {
            throttle = worldPhysics.getConstantSpeedThrottle(prediction.getSpeed());
        }
        prediction = prediction.move(throttle);
        return prediction;
    }

    @Override
    public void processCarCrash(Crash whoCrashed) {
        cleanLap = world.getMyCarData().getPos().piecePosition.lap + 1;
        cleanLapPosition = world.getMyCarData().getPosInBaseLane(ALLOW_APPROXIMATE_VALUES);
        lapMaxAngle = 0;
    }

    @Override
    protected void processCarSpawn(Spawn whoSpawned) {

    }

    @Override
    public void startRace() {
        super.startRace();
        init();
    }

    @Override
    public void finish() {
        super.finish();
        worldPhysics.logParams();
        if (DUMP_SAMPLES) {
            Log.log("Dumping WorldPhysics samples to samples.dump");
            try {
                PrintWriter writer = new PrintWriter("samples.dump");
                worldPhysics.dumpSamples(writer);
                writer.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
