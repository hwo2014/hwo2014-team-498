package noobbot.logic;

import noobbot.io.Log;
import noobbot.model.CarPosition;
import noobbot.model.Track;

public class CarDataUtils {

    // Global value for EPS
    public static final double EPS = 1e-8;

    // Number of segments in the base lane
    public static final int SEGMENTS_IN_BASE_LANE = 5000;

    /**
     * This method returns the fair radius of the track piece
     */
    public static double getActualPieceRadius(World world, int pieceIdx, int laneStartIdx, int laneEndIdx) {
        Track.Piece piece = world.getRace().track.pieces[pieceIdx];
        Track.Lane lane1 = world.getRace().track.getLaneByIndex(laneStartIdx);
        Track.Lane lane2 = world.getRace().track.getLaneByIndex(laneEndIdx);
        return 0.5 * (piece.getLaneRadius(lane1) + piece.getLaneRadius(lane2)) * (piece.angle != null ? Math.signum(piece.angle) : 1.0);
    }

    /**
     * This method returns the real fair driving distance from the current pos to the end of the current piece
     */
    public static double getActualDistanceToPieceEnd(World world, CarPosition pos, boolean approximationAllowed) {
        CarPosition.PiecePosition piecePos = pos.piecePosition;
        Track.Piece piece = world.getRace().track.getPieceByIndex(piecePos.pieceIndex);

        // get current position in piece
        double posInPiece = piecePos.inPieceDistance;

        // get total piece length
        double totalPieceLen = piece.getLength(piecePos.lane.startLaneIndex, piecePos.lane.endLaneIndex, approximationAllowed);

        // not sure how NaNs will be handled later, so let's exit now if we are not satisfied
        if (!approximationAllowed && Double.isNaN(totalPieceLen)) {
            return Double.NaN;
        }

        if (posInPiece < -EPS || posInPiece > totalPieceLen + EPS) {
            Log.error(String.format("Exceeding position in piece %.2f vs %.2f: %s", posInPiece, totalPieceLen, piece));
        }
        return Math.max(0.0, Math.min(totalPieceLen - posInPiece, totalPieceLen));
    }

    /**
     * This method gets current car position on the track, mapped to "base lane"
     */
    public static double getCarPositionInBaseLane(World world, CarPosition pos, boolean approximationAllowed) {
        CarPosition.PiecePosition piecePos = pos.piecePosition;
        Track.Piece piece = world.getRace().track.getPieceByIndex(piecePos.pieceIndex);

        // add all previous pieces, mapped to the "base lane"
        double result = piecePos.pieceIndex - 1 >= 0 ? world.getSumDistInBaseLaneByPiece(piecePos.pieceIndex - 1) : 0;

        // add current piece, mapped to the "base lane"
        double posInPiece = piecePos.inPieceDistance;

        // get total piece length
        double totalPieceLen = piece.getLength(piecePos.lane.startLaneIndex, piecePos.lane.endLaneIndex, approximationAllowed);

        // not sure how NaNs will be handled later, so let's exit now if we are not satisfied
        if (!approximationAllowed && Double.isNaN(totalPieceLen)) {
            return Double.NaN;
        }

        double totalBasePieceLen = piece.getSegmentLength(world.getRace().track.getBaseLane());
        result += (posInPiece / totalPieceLen) * totalBasePieceLen;

        if (result - EPS > getTotalTrackLengthInBaseLane(world)) {
            throw new IllegalStateException(String.format("Our car position on the track somehow exceeds the length of the track: %.2f vs %.2f", result, getTotalTrackLengthInBaseLane(world)));
        }

        return result;
    }

    /**
     * This method gets track length in the "base lane"
     */
    public static double getTotalTrackLengthInBaseLane(World world) {
        return world.getSumDistInBaseLaneByPiece(world.getRace().track.pieces.length - 1);
    }

    /**
     * This methods maps car position into a base lane and returns ID of the corresponding segment
     */
    protected static int getCarSegmentIdxInBaseLane(World world, CarPosition pos, boolean approximationAllowed) {
        double myPositionAtTrack = getCarPositionInBaseLane(world, pos, approximationAllowed);

        // not sure how NaNs will be handled later, so let's exit now if we are not satisfied
        if (!approximationAllowed && Double.isNaN(myPositionAtTrack)) {
            return Integer.MIN_VALUE;
        }

        double trackLen = CarDataUtils.getTotalTrackLengthInBaseLane(world);
        return getSegmentIdx(myPositionAtTrack, trackLen);
    }

    /**
     * This method returns the distance traveled between two car positions
     * Essentially this represents speed of the car, when calculated between two subsequent ticks
     * It takes into account piece changes and different piece types
     */
    public static double getDistanceTraveledBetweenPositions(World world, CarPosition prevPos, CarPosition pos, boolean approximationAllowed) {
        // if we are still in the same piece, then it's easy
        if (pos.piecePosition.pieceIndex == prevPos.piecePosition.pieceIndex) {
            return Math.abs(pos.piecePosition.inPieceDistance - prevPos.piecePosition.inPieceDistance);
        } else {
            double result = 0.0;

            // we moved to one of the next pieces. this means that we drove through the remainder of the last piece
            double distanceToPieceEnd = getActualDistanceToPieceEnd(world, prevPos, approximationAllowed);

            // not sure how NaNs will be handled later, so let's exit now if we are not satisfied
            if (!approximationAllowed && Double.isNaN(distanceToPieceEnd)) {
                return Double.NaN;
            }

            result += distanceToPieceEnd;

            // let's see if there is anything in the middle (pieces that we fully passed)
            int piecesCnt = world.getRace().track.pieces.length;
            int pieceIdx = prevPos.piecePosition.pieceIndex + 1;
            while ((pieceIdx % piecesCnt) != pos.piecePosition.pieceIndex) {
                // TODO: this will be called if we are not in adjacent pieces. this is just an approximation
                Track.Lane lane = world.getRace().track.getLaneByIndex(pos.piecePosition.lane.startLaneIndex);
                result += world.getRace().track.pieces[prevPos.piecePosition.pieceIndex].getSegmentLength(lane);
                pieceIdx++;
            }

            // the remainder
            result += pos.piecePosition.inPieceDistance;
            return result;
        }
    }

    /**
     * Returns distance traveled in the base lane between car positions
     */
    protected static double getDistanceTraveledBetweenPositionsInBaseLane(World world, CarPosition prevPos, CarPosition pos, boolean approximationAllowed) {
        double p1 = getCarPositionInBaseLane(world, pos, approximationAllowed);

        // not sure how NaNs will be handled later, so let's exit now if we are not satisfied
        if (!approximationAllowed && Double.isNaN(p1)) {
            return Double.NaN;
        }

        double p2 = getCarPositionInBaseLane(world, prevPos, approximationAllowed);

        // not sure how NaNs will be handled later, so let's exit now if we are not satisfied
        if (!approximationAllowed && Double.isNaN(p2)) {
            return Double.NaN;
        }

        double result = p1 - p2;
        if (result < 0) {
            result += getTotalTrackLengthInBaseLane(world);
        }
        return result;
    }

    /**
     * Returns ID of the segment in base lane
     */
    public static int getSegmentIdx(double carPos, double trackLen) {
        if (carPos < 0) {
            carPos += trackLen;
        }
        return (int) (SEGMENTS_IN_BASE_LANE * carPos / trackLen);
    }

}
