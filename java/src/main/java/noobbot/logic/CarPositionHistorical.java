package noobbot.logic;

import noobbot.model.CarPosition;

public class CarPositionHistorical {

    public final double exactSpeed;
    public final int lane;
    public final boolean isTurboActive;
    public final boolean isAlive;
    public final boolean isTrustable;

    public final int pieceIdx;
    public final int startLaneIdx;
    public final int endLaneIdx;
    public final double distInPiece;

    public CarPositionHistorical(CarPosition pos, double exactSpeed, boolean isTurboActive, boolean isAlive, boolean isThrustable) {
        this.lane = pos.piecePosition.lane.startLaneIndex;
        this.exactSpeed = exactSpeed;
        this.isTurboActive = isTurboActive;
        this.isAlive = isAlive;
        this.isTrustable = isThrustable;

        this.pieceIdx = pos.piecePosition.pieceIndex;
        this.startLaneIdx = pos.piecePosition.lane.startLaneIndex;
        this.endLaneIdx = pos.piecePosition.lane.endLaneIndex;
        this.distInPiece = pos.piecePosition.inPieceDistance;
    }

}
