package noobbot.logic;

import noobbot.io.Log;
import noobbot.logic.physics.Prediction;
import noobbot.logic.physics.WorldPhysics;
import noobbot.model.CarPosition;
import noobbot.model.Track;
import noobbot.model.TurboAvailable;

import java.util.ArrayList;

public class TurboManager {

    private static final boolean ALLOW_APPROXIMATE_VALUES = true;

    private final World world;
    private final WorldPhysics physics;
    private final RacingLine racingLine;
    private final PhysicsStrategy strategy;

    private double[] savedTime;
    private ArrayList<Prediction> baselinePredictions;
    private int basePieceIndex;

    public TurboManager(World world, WorldPhysics physics, RacingLine racingLine, PhysicsStrategy strategy) {
        this.world = world;
        this.physics = physics;
        this.racingLine = racingLine;
        this.strategy = strategy;
        Thread thread = new Thread("turbo") {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    try {
                        processTurboLap();
                    } catch (Exception e) {
                        if (e instanceof InterruptedException) {
                            return;
                        }
                        Log.error("Error in turbo thread: ", e);
                    }
                }
            }
        };
        thread.setDaemon(true);
        thread.start();
    }

    public boolean isGoodTimeForTurbo() {
        CarData myCarData = world.getMyCarData();
        CarPosition.PiecePosition myPiecePosition = myCarData.getPos().piecePosition;

        TurboAvailable turboAvailable = myCarData.getTurboAvailable();
        if (turboAvailable == null || !physics.isTrustable() || baselinePredictions == null) {
            return false;
        }

        final ArrayList<Prediction> baselinePredictions;
        final double[] savedTime;
        final int basePieceIndex;
        synchronized (this) {
            baselinePredictions = this.baselinePredictions;
            savedTime = this.savedTime;
            basePieceIndex = this.basePieceIndex;
        }

        int bestTurboTick = -1;
        double bestTurboSaving = Double.NEGATIVE_INFINITY;
        double weight = 1.0;
        for (int t = 0; t < baselinePredictions.size() - 1; t++) {
            Prediction nextPrediction = baselinePredictions.get(t + 1);
            int nLap = nextPrediction.getStartLap();
            int nSegmentIndex = nextPrediction.getSegmentIndex() + basePieceIndex;

            // normalizing nSegmentIndex and nLap
            while (nSegmentIndex >= world.getRace().track.pieces.length) {
                nSegmentIndex -= world.getRace().track.pieces.length;
                nLap++;
            }
            double nSegmentPos = nextPrediction.getSegmentPos();
            if (nLap < myPiecePosition.lap
                    || nLap == myPiecePosition.lap && nSegmentIndex < myPiecePosition.pieceIndex
                    || nLap == myPiecePosition.lap && nSegmentIndex == myPiecePosition.pieceIndex
                    && nSegmentPos < myPiecePosition.inPieceDistance) {
                continue;
            }
            if (bestTurboTick == -1) {
                bestTurboTick = t;
                bestTurboSaving = savedTime[t] * weight;
            } else if (bestTurboSaving < savedTime[t] * weight) {
                return false;
            }
            // reducing weight (it will be ~0.67 after 1000 ticks) so we prefer to use turbo earlier
            weight *= 0.9996;
        }
        return true;
    }

    public void processTurboLap() throws InterruptedException {
        CarData myCarData = world.getMyCarData();
        CarPosition myCarPos = myCarData.getPos();

        TurboAvailable turboAvailable = myCarData.getTurboAvailable();
        if (turboAvailable == null || !physics.isTrustable()) {
            synchronized (this) {
                baselinePredictions = null;
                savedTime = null;
            }
            Thread.sleep(1);
            return;
        }

        int lap = myCarPos.piecePosition.lap;

        int pieceIndex = myCarPos.piecePosition.pieceIndex;
        int trackPieceCount = world.getRace().track.pieces.length;
//        Log.log("Starting processing turbo lap at lap=" + lap + ", piece=" + pieceIndex);

        int[] lanes = racingLine.getNextLanes(myCarPos, false);

        double myAngle = myCarData.getAngle();
        double myAngleSpeed = myCarData.getAngleSpeed();
        double mySpeed = myCarData.getSpeed(false);
        if (Double.isNaN(mySpeed)) {
            // if our speed is unreliable, trying to calculate it based on the previous speed and throttle
            double prevSpeed = myCarData.getPrevSpeed(false);
            if (Double.isNaN(prevSpeed)) {
                mySpeed = myCarData.getSpeed(true); // approximate
            } else {
                mySpeed = physics.getSpeedAfterThrottle(
                        prevSpeed, myCarData.getLastThrottleSent() * myCarData.getPrevTurboFactor(), 1);
            }
        }

        ArrayList<Double> pPieceLength = new ArrayList<>();
        ArrayList<RadiusProvider> pPieceRadii = new ArrayList<>();

        int offset = 0;

        // TODO: logic for checking last lap should be different in qualification
        int laps = world.getRace().raceSession.laps == 0 ? Integer.MAX_VALUE : world.getRace().raceSession.laps;

        // if we're still not at the end of a track, need to handle this differently
        boolean lastLap = lap >= laps - 1;

        while (offset < world.getRace().track.pieces.length && offset + 1 < lanes.length
                && lap + (pieceIndex + offset) / trackPieceCount < laps) {

            int offsetPieceIdx = (pieceIndex + offset) % trackPieceCount;
            Track.Piece piece = world.getRace().track.getPieceByIndex(offsetPieceIdx);

            double pieceLen = piece.getLength(lanes[offset], lanes[offset + 1], ALLOW_APPROXIMATE_VALUES);
            pPieceLength.add(pieceLen);

            pPieceRadii.add(physics.getRadiusProvider(piece, lanes[offset], lanes[offset + 1]));

            offset++;
        }

        double[] segmentLengths = new double[pPieceLength.size()];
        RadiusProvider[] segmentRadii = new RadiusProvider[pPieceLength.size()];
        for (int i = 0; i < segmentLengths.length; i++) {
            segmentLengths[i] = pPieceLength.get(i);
            segmentRadii[i] = pPieceRadii.get(i);
        }

        double startAt = myCarPos.piecePosition.inPieceDistance;

        Prediction prediction = physics.getPrediction(
                segmentLengths, segmentRadii, startAt, lap, mySpeed, myAngle, myAngleSpeed);

        ArrayList<Prediction> baselinePredictions = new ArrayList<>();
        while (!prediction.hasReachedEnd()) {
            baselinePredictions.add(prediction);
            prediction = bestMove(prediction, 1.0, lastLap);
        }
        baselinePredictions.add(prediction);

        double[] savedTime = new double[baselinePredictions.size()];

        for (int t = 0; t < baselinePredictions.size(); t++) {
            double turboDistance = 0;
            Prediction turboPrediction = baselinePredictions.get(t);

            // skipping 1 tick, turbo will be still inactive
            int turboTicks = 1;
            turboPrediction = bestMove(turboPrediction, 1.0, lastLap);

            for (int i = 0; i < turboAvailable.getTurboDurationTicks() + 1 && !turboPrediction.hasReachedEnd(); i++) {
                turboPrediction = bestMove(turboPrediction, turboAvailable.getTurboFactor(), lastLap);
                turboDistance += turboPrediction.getSpeed();
                turboTicks += 1;
            }
            // continue moving to let our speed return to normal values
            for (int i = 0; i < turboAvailable.getTurboDurationTicks() * 2 && !turboPrediction.hasReachedEnd(); i++) {
                turboPrediction = bestMove(turboPrediction, 1.0, lastLap);
                turboDistance += turboPrediction.getSpeed();
                turboTicks += 1;
            }

            double baselineDistance = 0;
            int baselineTicks = 0;
            for (int i = 0; i < baselinePredictions.size() && baselineDistance < turboDistance; i++) {
                baselineTicks++;
                baselineDistance += baselinePredictions.get(i).getSpeed();
                if (i + 1 < baselinePredictions.size() && baselinePredictions.get(i + 1).hasReachedEnd()) {
                    break;
                }
            }

            savedTime[t] = baselineTicks - turboTicks;
        }

        synchronized (this) {
            this.savedTime = savedTime;
            this.baselinePredictions = baselinePredictions;
            this.basePieceIndex = pieceIndex;
        }

//        Log.log("Finished processing turbo lap");
    }

    private Prediction bestMove(Prediction prediction, double turboFactor, boolean isLastLap) {
        Prediction bestCase = prediction.move(turboFactor);
        if (isGood(bestCase, isLastLap)) {
            return bestCase;
        } else {
            bestCase = prediction.move(turboFactor * 0.5);
            if (isGood(bestCase, isLastLap)) {
                return bestCase;
            } else {
                return prediction.move(0.0);
            }
        }
    }

    private boolean isGood(Prediction prediction, boolean isLastLap) {
        if (strategy.findMaxAbsAngle(prediction) > physics.getCriticalAngle()) {
            return false;
        }
        // if it's not our last lap and we reached the end, we don't know if it's good, so return false
        return isLastLap || !prediction.hasReachedEnd();
    }
}
