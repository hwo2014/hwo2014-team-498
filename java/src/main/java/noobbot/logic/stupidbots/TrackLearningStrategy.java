package noobbot.logic.stupidbots;

import noobbot.io.MessageSender;
import noobbot.logic.*;
import noobbot.model.CarPosition;
import noobbot.model.Crash;
import noobbot.model.Spawn;

import java.util.Arrays;

@SuppressWarnings("UnusedDeclaration")
public class TrackLearningStrategy extends Strategy {

    double[] throttle = new double[CarDataUtils.SEGMENTS_IN_BASE_LANE];

    public TrackLearningStrategy(World world, MessageSender sender) {
        super(world, sender);
        init();
    }

    private void init() {
        Arrays.fill(throttle, 1.0);
    }

    CarPosition myPos;
    double lastAngle, trackLenInBaseLane, distanceTraveledLastTickInBaseLane, myCarPositionInBaseLane, distToPieceEnd;

    @Override
    public Move processCarPositions() {

        // get position of our own car
        myPos = getWorld().getMyCarData().getPos();
        trackLenInBaseLane = CarDataUtils.getTotalTrackLengthInBaseLane(getWorld());
        myCarPositionInBaseLane = getWorld().getMyCarData().getPosInBaseLane(true);
        distanceTraveledLastTickInBaseLane = getWorld().getMyCarData().getSpeedInBaseLane();

        // call this just for the point validating the current position
        distToPieceEnd = CarDataUtils.getActualDistanceToPieceEnd(getWorld(), myPos, true);

        // find the optimal lane for the next piece
        ShortestRacingLine racingLine = new ShortestRacingLine(getWorld());
        int[] lanes = racingLine.getNextLanes(myPos, false);
        int optimalLaneNextSwitch = racingLine.getLaneAfterNextSwitch(myPos, lanes);

        // see where we are right now
        int segmentIdx = getWorld().getMyCarData().getSegmentIdxInBaseLane(true);

        // see if absolute angle is increasing or decreasing
        boolean angleIncreasing = (Math.abs(myPos.angle) > 1.0) && (Math.abs(myPos.angle) > Math.abs(lastAngle) + CarDataUtils.EPS);
        lastAngle = myPos.angle;

        Move move = new Move();
        move.setLaneChange(optimalLaneNextSwitch);
        if (distanceTraveledLastTickInBaseLane < 2.0) {
            // make sure we don't get stuck if we respawn and throttle[segmentIdx] == 0
            move.setThrottle(1.0);
        } else if (angleIncreasing) {
            // slow when angle is increasing
            move.setThrottle(0.1);
        } else {
            // apply throttle per our plan
            move.setThrottle(throttle[segmentIdx]);
        }
        return move;
    }

    @Override
    public void processCarCrash(Crash whoCrashed) {
        if (whoCrashed.equals(getWorld().getMyCar())) {
            // stupid braking, no physics model behind it
            double distanceToBreak = 30.0;
            int brakingSegmentIdx = CarDataUtils.getSegmentIdx(myCarPositionInBaseLane - distanceToBreak * distanceTraveledLastTickInBaseLane, trackLenInBaseLane);
            int currentSegmentIdx = getWorld().getMyCarData().getSegmentIdxInBaseLane(true);
            System.out.printf("Reducing throttle at segment range [%d, %d]\n", brakingSegmentIdx, currentSegmentIdx);

            int idx = brakingSegmentIdx;
            while (idx != currentSegmentIdx) {
                throttle[idx] = 0.0;
                idx++;
                if (idx >= CarDataUtils.SEGMENTS_IN_BASE_LANE) {
                    idx = 0;
                }
            }
        }
    }

    @Override
    protected void processCarSpawn(Spawn whoSpawned) {

    }

}
