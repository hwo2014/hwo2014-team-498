package noobbot.logic.stupidbots;

import noobbot.io.MessageSender;
import noobbot.logic.*;
import noobbot.model.CarPosition;
import noobbot.model.Crash;
import noobbot.model.Spawn;

public class ConstantThrottleBot extends Strategy {

    private RacingLine racingLine;
    private double throttle;

    public ConstantThrottleBot(World world, MessageSender messageSender, double throttle) {
        super(world, messageSender);
        this.throttle = throttle;
    }

    public void useShortestRacingLine() {
        this.racingLine = new ShortestRacingLine(getWorld());
    }

    @Override
    protected Move processCarPositions() {
        /*
        double carL = getWorld().getRace().cars[getWorld().getMyCarID()].dimensions.length;
        double veryCloseDistance = 1.15 * carL;
        if (getWorld().getMyCarData().getPos().id.name.contains("32")) {
            for (CarData data : getWorld().getCarDataAsArray())
                if (!data.getPos().id.equals(getWorld().getMyCarData().getPos().id)) {

                    if (getWorld().getMyCarData().gotStuckBehind(data, veryCloseDistance)) {
                        Log.error("Stuck!!!");
                    }

                }
        }
        */

        Move move = new Move();
        move.setThrottle(throttle);
        if (racingLine != null) {
            CarPosition pos = getWorld().getMyCarData().getPos();
            int[] lanes = racingLine.getNextLanes(pos, false);
            move.setLaneChange(racingLine.getLaneAfterNextSwitch(pos, lanes));
        }
        return move;
    }

    @Override
    protected void processCarCrash(Crash whoCrashed) {
    }

    @Override
    protected void processCarSpawn(Spawn whoSpawned) {
    }

}
