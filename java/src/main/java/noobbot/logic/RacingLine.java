package noobbot.logic;

import noobbot.model.CarPosition;
import noobbot.model.Track;

public abstract class RacingLine {

    private World world;

    protected RacingLine(World world) {
        this.world = world;
    }

    public World getWorld() {
        return world;
    }

    protected abstract int[] calcNextLanes(CarPosition pos, boolean overtake);

    // This method returns an array of optimal lane changes
    //  * 0th element -- contains the current lane for the current piece (lane in which we are in right now)
    //  * 1th element -- contains optimal lane for next piece
    //  * ..
    public int[] getNextLanes(CarPosition pos, boolean overtake) {
        int[] lanes = calcNextLanes(pos, overtake);
        validateLanes(pos, lanes);
        return lanes;
    }

    // This method just returns the optimal lane for next piece (where we want to be at next piece)
    public int getLaneAfterNextSwitch(CarPosition pos, int[] lanes) {
        int myPieceIdx = pos.piecePosition.pieceIndex;

        Track.Piece[] pieces = world.getRace().track.pieces;
        int piecesCnt = pieces.length;

        for (int i = 0; i < piecesCnt; i++) {
            int idx = (myPieceIdx + i) % piecesCnt;
            if (pieces[idx].hasSwitch) {
                return lanes[i + 1];
            }
        }

        return pos.piecePosition.lane.endLaneIndex;
    }

    // Validation method for lanes
    private void validateLanes(CarPosition myPos, int[] lanes) {
        int myPieceIdx = myPos.piecePosition.pieceIndex;

        Track.Piece[] pieces = world.getRace().track.pieces;
        int piecesCnt = pieces.length;

        for (int i = 0; i < lanes.length - 1; i++)
            if (lanes[i] != lanes[i + 1]) {
                int idx = (myPieceIdx + i) % piecesCnt;
                if (!pieces[idx].hasSwitch) {
                    throw new IllegalStateException("Attempting to switch lanes, but it's not a switch!");
                }
            }
    }


}
