package noobbot.logic;

public interface RadiusProvider {
    double getRadius(double inPieceDistance);

    /**
     * Returns {@code true} when there is a good approximation. Otherwise it just returns a minimum of 2 radii.
     */
    boolean isRadiusKnown();
}
