package noobbot.logic;

public class Move {

    private int laneIdx = Integer.MIN_VALUE;
    private boolean turboEnabled = false;
    private double throttle = Double.MIN_VALUE;

    private boolean forceThrottleToBeApplied = false;

    public int getLaneIdx() {
        return laneIdx;
    }

    public boolean isTurboEnabled() {
        return turboEnabled;
    }

    public double getThrottle() {
        return throttle;
    }

    public void setLaneChange(int laneIdx) {
        this.laneIdx = laneIdx;
    }

    public void setTurboEnabled() {
        this.turboEnabled = true;
    }

    public void setThrottle(double throttle) {
        this.throttle = throttle;
    }

    public boolean isForceThrottleToBeApplied() {
        return forceThrottleToBeApplied;
    }

    public void setForceThrottleToBeApplied(boolean forceThrottleToBeApplied) {
        this.forceThrottleToBeApplied = forceThrottleToBeApplied;
    }


}
