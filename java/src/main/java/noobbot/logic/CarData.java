package noobbot.logic;

import noobbot.io.Log;
import noobbot.logic.physics.WorldPhysics;
import noobbot.model.CarPosition;
import noobbot.model.Track;
import noobbot.model.TurboAvailable;

import java.util.*;

@SuppressWarnings("UnusedDeclaration")
public class CarData {

    private boolean crashed = false;
    private boolean finished = false;

    private double speedCalc;
    private double speedApprox;

    private double prevSpeedCalc;
    private double prevSpeedApprox;

    private double accelerationCalc;
    private double accelerationApprox;

    private double angle;
    private double angleSpeed;
    private double angleAcceleration;

    private double posInBaseLaneCalc;
    private double posInBaseLaneApprox;

    private int segmentIdxInBaseLaneCalc;
    private int segmentIdxInBaseLaneApprox;

    private double speedInBaseLane;

    private CarPosition prevPos;
    private CarPosition pos;

    private double prevAngle;
    private double prevAngleSpeed;

    private double prevPosInBaseLaneCalc;
    private double prevPosInBaseLaneApprox;

    private int prevSegmentIdxInBaseLaneCalc;
    private int prevSegmentIdxInBaseLaneApprox;

    private int lane;
    private int prevLane = -1;

    private double turboFactor = 1.0;
    private double prevTurboFactor;

    private double lastThrottleSent = -1;
    private int lastSwitchLaneSent = -1;

    private World world;

    // this will be used to predict which lane the opponent will take
    private int[] lastLaneForPiece;

    // historical positions
    public static final int MAX_HISTORY_SIZE = 1000;
    private LinkedList<CarPositionHistorical> historicalPos = new LinkedList<>();

    public CarData(World world) {
        this.world = world;

        this.lastLaneForPiece = new int[world.getRace().track.pieces.length];
        Arrays.fill(lastLaneForPiece, -1);

        resetCarParameters();
    }

    public void processCarPosition(CarPosition updatedPos) {
        // update prevs
        updatePrevs();

        // assign position & lane
        pos = updatedPos;
        lane = updatedPos.piecePosition.lane.startLaneIndex;
        lastLaneForPiece[pos.piecePosition.pieceIndex] = lane;

        // 0th degree params
        angle = pos.angle;

        posInBaseLaneCalc = CarDataUtils.getCarPositionInBaseLane(world, pos, false);
        posInBaseLaneApprox = CarDataUtils.getCarPositionInBaseLane(world, pos, true);

        segmentIdxInBaseLaneCalc = CarDataUtils.getCarSegmentIdxInBaseLane(world, pos, false);
        segmentIdxInBaseLaneApprox = CarDataUtils.getCarSegmentIdxInBaseLane(world, pos, true);

        // 1st degree params
        if (prevPos != null) {
            speedCalc = CarDataUtils.getDistanceTraveledBetweenPositions(world, prevPos, pos, false);
            speedApprox = CarDataUtils.getDistanceTraveledBetweenPositions(world, prevPos, pos, true);

            speedInBaseLane = CarDataUtils.getDistanceTraveledBetweenPositionsInBaseLane(world, prevPos, pos, true);

            angleSpeed = angle - prevAngle;

            // 2nd degree params
            angleAcceleration = angleSpeed - prevAngleSpeed;

            accelerationCalc = speedCalc - prevSpeedCalc;
            accelerationApprox = speedApprox - prevSpeedApprox;
        }

        CarPositionHistorical hPos = new CarPositionHistorical(pos, speedCalc, prevTurboFactor > 1.0, isAlive(), world.getPhysics() != null && world.getPhysics().isTrustable());
        historicalPos.addLast(hPos);
        if (historicalPos.size() > MAX_HISTORY_SIZE) {
            historicalPos.removeFirst();
        }

        if (updatedPos.id.equals(world.getMyCar())) {
            turboFactor = getTurboFactor();

            // if we just changed lanes, then lane change is no longer pending
            if (lane != prevLane) {
                Log.log("Lane change completed, now our car is in lane " + lane);
                setLastSwitchLaneSent(-1);
                updateSwitchPieceLength();
            }

            // if we passed a switch and lane change didn't happen for some reason, it's really bad. We should try again
            if (prevPos != null && world.getRace().track.getPieceByIndex(prevPos.piecePosition.pieceIndex).hasSwitch &&
                prevPos.piecePosition.pieceIndex != pos.piecePosition.pieceIndex && getLastSwitchLaneSent() != -1) {
                Log.error("We passed a switch, but lane change didn't happen. This is pretty bad. Let's reset and retry lane switch later!");
                setLastSwitchLaneSent(-1);
            }
        }
    }

    private void updateSwitchPieceLength() {
        int pieceCnt = world.getRace().track.pieces.length;

        if (prevPos == null) {
            // this sometimes happens when there is no previous segment
            return;
        }

        if ((prevPos.piecePosition.pieceIndex + 1) % pieceCnt != pos.piecePosition.pieceIndex) {
            // ideally we should only come here when prev and current pieces are adjacent
            // if it's not the case, let's just exit
            return;
        }

        Track.Piece prevPiece = world.getRace().track.pieces[prevPos.piecePosition.pieceIndex];
        if (!prevPiece.hasSwitch) {
            // if we just changed lanes, then the previous piece must be a switch
            // if it's not the case (that would be very unbelievable), let's just exit
            return;
        }

        WorldPhysics physics = world.getPhysics();
        if (physics == null) {
            // if no physics is available, let's exit
            return;
        }

        if (!physics.isTrustable()) {
            // if physics is not properly initialized, then let's exit as well
            // Log.important("[switchlen] Physics is not properly initialized yet, skipping switch");
            return;
        }

        if (Double.isNaN(prevSpeedCalc)) {
            // throw new IllegalStateException("Something is fundamentally wrong here. Previous speed should always be available here");
            return;
        }

        double vPredicted = physics.getSpeedAfterThrottle(prevSpeedCalc, lastThrottleSent * prevTurboFactor, 1.0);
        double switchLenCalculated = prevPos.piecePosition.inPieceDistance + vPredicted - pos.piecePosition.inPieceDistance;
        double switchLengthApprox = prevPiece.getLength(prevLane, lane, true);

        String message = null;
        if (Math.abs(switchLenCalculated - switchLengthApprox) > CarDataUtils.EPS) {
            message = String.format(
                "Piece %d %s switch, %d -> %d: calculated %.6f vs. previous %.6f %s DFC %.6f -> %.6f",
                prevPos.piecePosition.pieceIndex,
                prevPiece.radius != null ? "curved" : "straight",
                prevLane,
                lane,
                switchLenCalculated,
                switchLengthApprox,
                prevTurboFactor > 1.0 ? "(turbo is enabled!)" : "",
                world.getRace().track.lanes[prevLane].distanceFromCenter,
                world.getRace().track.lanes[lane].distanceFromCenter
            );
        }

        prevPiece.foundSwitchLength(world, prevLane, lane, switchLenCalculated, message);
    }

    private void updatePrevs() {
        prevPos = pos;
        prevAngle = angle;
        prevSpeedCalc = speedCalc;
        prevSpeedApprox = speedApprox;
        prevAngleSpeed = angleSpeed;
        prevPosInBaseLaneCalc = posInBaseLaneCalc;
        prevPosInBaseLaneApprox = posInBaseLaneApprox;
        prevSegmentIdxInBaseLaneCalc = segmentIdxInBaseLaneCalc;
        prevSegmentIdxInBaseLaneApprox = segmentIdxInBaseLaneApprox;
        prevLane = lane;
        prevTurboFactor = turboFactor;
    }

    /**
     * Clear all car parameters except its position (useful e.g. when it crashes)
     */
    private void resetCarParameters() {
        speedCalc = 0.0;
        speedApprox = 0.0;
        angle = 0.0;
        angleSpeed = 0.0;
        angleAcceleration = 0.0;
        accelerationCalc = 0.0;
        accelerationApprox = 0.0;
        speedInBaseLane = 0.0;
        lastThrottleSent = -1; // we don't necessarily need this, but we want to be 100% sure we start accelerating after a crash
        turboFactor = 1.0;

        updatePrevs();

        if (isTurboActive()) {
            markTurboEnd();
        }
    }

    public CarPosition getPos() {
        return pos;
    }

    public CarPosition getPrevPos() {
        return prevPos;
    }

    public double getAngle() {
        return angle;
    }

    public double getAngleSpeed() {
        return angleSpeed;
    }

    public double getAngleAcceleration() {
        return angleAcceleration;
    }

    public double getPrevAngle() {
        return prevAngle;
    }

    public double getPrevAngleSpeed() {
        return prevAngleSpeed;
    }

    public double getSpeed(boolean approximationAllowed) {
        double result = speedCalc;
        if (Double.isNaN(result) && approximationAllowed) {
            result = speedApprox;
        }
        return result;
    }

    public double getPosInBaseLane(boolean approximationAllowed) {
        double result = posInBaseLaneCalc;
        if (Double.isNaN(result) && approximationAllowed) {
            result = posInBaseLaneApprox;
        }
        return result;
    }

    public int getSegmentIdxInBaseLane(boolean approximationAllowed) {
        int result = segmentIdxInBaseLaneCalc;
        if (result == Integer.MIN_VALUE && approximationAllowed) {
            result = segmentIdxInBaseLaneApprox;
        }
        return result;
    }

    public double getPrevPosInBaseLane(boolean approximationAllowed) {
        double result = prevPosInBaseLaneCalc;
        if (Double.isNaN(result) && approximationAllowed) {
            result = prevPosInBaseLaneApprox;
        }
        return result;
    }

    public int getPrevSegmentIdxBaseLane(boolean approximationAllowed) {
        int result = prevSegmentIdxInBaseLaneCalc;
        if (result == Integer.MIN_VALUE && approximationAllowed) {
            result = prevSegmentIdxInBaseLaneApprox;
        }
        return result;
    }

    public double getSpeedInBaseLane() {
        return speedInBaseLane;
    }

    public int getLane() {
        return lane;
    }

    public int getPrevLane() {
        return prevLane;
    }

    public double getAcceleration(boolean approximationAllowed) {
        double result = accelerationCalc;
        if (Double.isNaN(result) && approximationAllowed) {
            result = accelerationApprox;
        }
        return result;
    }

    public double getPrevSpeed(boolean approximationAllowed) {
        double result = prevSpeedCalc;
        if (Double.isNaN(result) && approximationAllowed) {
            result = prevSpeedApprox;
        }
        return result;
    }

    private TurboAvailable turboAvailable;
    private TurboAvailable activeTurbo;
    private int currentTurboLap;
    private double currentTurboStart;
    private Map<Integer, List<double[]>> turboActivations = new HashMap<>();

    public void setTurboAvailable(TurboAvailable turboAvailable) {
        this.turboAvailable = turboAvailable;
    }

    public TurboAvailable getTurboAvailable() {
        if (!isAlive()) {
            // if we just crashed, we obviously can't use turbo
            return null;
        }
        if (isTurboActive()) {
            // we are using turbo right now already, so can't use the second turbo
            return null;
        }
        return turboAvailable;
    }

    public void markTurboStart() {
        activeTurbo = turboAvailable;
        currentTurboStart = getPosInBaseLane(true); // this is for charts only, so approximate is fine
        currentTurboLap = getPos().piecePosition.lap;
        turboAvailable = null;
    }

    public void markTurboEnd() {
        if (currentTurboLap == getPos().piecePosition.lap) {
            // same lap turbo
            addTurboActivation(currentTurboLap, currentTurboStart, getPosInBaseLane(true)); // this is for charts only, so approximate is fine
        } else if (currentTurboLap + 1 == getPos().piecePosition.lap) {
            // multilap turbo
            addTurboActivation(currentTurboLap, currentTurboStart, CarDataUtils.getTotalTrackLengthInBaseLane(world));
            addTurboActivation(currentTurboLap + 1, 0.0, getPosInBaseLane(true)); // this is for charts only, so approximate is fine
        } else {
            Log.error("Drove more than 1 full lap on turbo!");
        }

        if (world.getMyCar().equals(pos.id)) {
            Log.log("Logged turbo usage for our car: [" + currentTurboStart + ", " + getPosInBaseLane(true) + "]"); // this is for charts only, so approximate is fine
        }

        activeTurbo = null;
    }

    private void addTurboActivation(int lap, double turboStartPos, double turboEndPos) {
        List<double[]> values = turboActivations.get(lap);
        if (values == null) {
            values = new ArrayList<>();
            turboActivations.put(lap, values);
        }
        values.add(new double[]{turboStartPos, turboEndPos});
    }

    public Map<Integer, List<double[]>> getTurboActivations() {
        return turboActivations;
    }

    public boolean isTurboActive() {
        return activeTurbo != null;
    }

    public double getTurboFactor() {
        if (!isTurboActive()) {
            return 1.0;
        }
        return activeTurbo.getTurboFactor();
    }

    public void markAsFinished() {
        finished = true;
    }

    public void markAsCrashed() {
        resetCarParameters();
        crashed = true;
    }

    public void markAsSpawned() {
        crashed = false;
    }

    public boolean isCrashed() {
        return crashed;
    }

    public boolean isFinished() {
        return finished;
    }

    public boolean isAlive() {
        return !crashed && !finished;
    }

    public void setLastThrottleSent(double lastThrottleSent) {
        this.lastThrottleSent = lastThrottleSent;
    }

    public double getLastThrottleSent() {
        return lastThrottleSent;
    }

    public int getLastSwitchLaneSent() {
        return lastSwitchLaneSent;
    }

    public void setLastSwitchLaneSent(int lastSwitchLaneSent) {
        this.lastSwitchLaneSent = lastSwitchLaneSent;
    }

    public double getPrevTurboFactor() {
        return prevTurboFactor;
    }

    public int[] getLastLaneForPiece() {

        // useful on all laps (including lap 1)
        // it prolongs opponent's line until next switch
        int pieceIdx = pos.piecePosition.pieceIndex;
        int nextLane = pos.piecePosition.lane.endLaneIndex;
        Track.Piece[] pieces = world.getRace().track.pieces;

        for (int i = 0; i < pieces.length; i++) {
            pieceIdx = (pieceIdx + 1) % pieces.length;
            lastLaneForPiece[pieceIdx] = nextLane;
            if (pieces[pieceIdx].hasSwitch) {
                // we don't know where the opponent will go after
                break;
            }
        }

        return lastLaneForPiece;
    }

    public LinkedList<CarPositionHistorical> getHistoricalPos() {
        return historicalPos;
    }

    public boolean wayFasterThan(CarData that) {
        int lookBack = 150;
        int atLeast = 125;
        LinkedList<CarPositionHistorical> h1 = this.getHistoricalPos();
        LinkedList<CarPositionHistorical> h2 = that.getHistoricalPos();
        double dist1 = 0;
        double dist2 = 0;
        int samples = 0;
        if (h1.size() > 0 && h2.size() > 0) {
            for (int i = 0; i < lookBack; i++) {
                if (h1.size() - 1 - i < 0) break;
                if (h2.size() - 1 - i < 0) break;

                CarPositionHistorical ch1 = h1.get(h1.size() - 1 - i);
                CarPositionHistorical ch2 = h2.get(h2.size() - 1 - i);

                // one for our car should be trustable
                if (!ch1.isTrustable && !ch2.isTrustable) continue;

                double s1 = ch1.exactSpeed;
                double s2 = ch2.exactSpeed;
                if (Double.isNaN(s1) || Double.isNaN(s2)) continue;

                boolean t1 = ch1.isTurboActive;
                boolean t2 = ch2.isTurboActive;
                if (t1 || t2) continue;

                if (s1 >= 0.0 && s2 >= 0.0) {
                    dist1 += s1;
                    dist2 += s2;
                    samples++;
                }
            }
        }

        double gap = 2.5 * world.getRace().cars[ world.getMyCarID() ].dimensions.length;
        return samples > atLeast && dist1 > dist2 + gap;
    }

    public boolean gotStuckBehind(CarData that, double veryCloseDistance) {
        int lookBack = 150;

        LinkedList<CarPositionHistorical> h1 = this.getHistoricalPos();
        LinkedList<CarPositionHistorical> h2 = that.getHistoricalPos();

        int stuckCnt = 0;
        int valid = 0;
        if (h1.size() > 0 && h2.size() > 0) {
            for (int i = 0; i < lookBack; i++) {
                if (h1.size() - 1 - i < 0) break;
                if (h2.size() - 1 - i < 0) break;

                CarPositionHistorical ch1 = h1.get(h1.size() - 1 - i);
                CarPositionHistorical ch2 = h2.get(h2.size() - 1 - i);

                if (ch1.pieceIdx != ch2.pieceIdx) continue;
                if (ch1.startLaneIdx != ch2.startLaneIdx) continue;
                if (ch1.endLaneIdx != ch2.endLaneIdx) continue;
                if (!ch1.isAlive || !ch2.isAlive) continue;

                /*
                if (i == 0 && getPos().id.name.contains("32")) {
                    if (ch2.distInPiece > ch1.distInPiece) {
                        Log.error("Distance: " + (ch2.distInPiece - ch1.distInPiece) + " vs. " + veryCloseDistance);
                    } else {
                        System.err.println("");
                    }
                }
                */

                valid++;

                if (ch2.distInPiece > ch1.distInPiece && ch2.distInPiece - ch1.distInPiece < veryCloseDistance) {
                    stuckCnt++;
                }
            }
        }

        // Log.error("Stuckcnt: " + stuckCnt + ", valid: " + valid);
        return valid > 40 && stuckCnt > valid / 3;
    }

}
