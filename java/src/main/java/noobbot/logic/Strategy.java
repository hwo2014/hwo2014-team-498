package noobbot.logic;

import noobbot.charts.ChartRenderer;
import noobbot.io.Log;
import noobbot.io.MessageSender;
import noobbot.model.*;

public abstract class Strategy {

    private ChartRenderer chartRenderer;
    protected World world;
    private MessageSender messageSender;
    private boolean justStarted;
    private boolean raceInProgress;

    protected Strategy(World world, MessageSender messageSender) {
        this.world = world;
        this.messageSender = messageSender;
    }

    protected World getWorld() {
        return world;
    }

    protected MessageSender getMessageSender() {
        return messageSender;
    }

    public void enableCharts() {
        chartRenderer = new ChartRenderer();
    }

    public void finish() {
        if (chartRenderer != null) {
            chartRenderer.setTurbo(getWorld().getMyCarData().getTurboActivations());
            chartRenderer.writeTo(
                String.format("charts/data-%s.js", getWorld().getRace().track.id),
                getWorld().getMyCar().name,
                getWorld().getRace().track.id
            );
        }
    }

    public final void handleCarPositions(CarPosition[] data) {
        // log initial car positions if the race has just started
        if (justStarted) {
            for (CarPosition p : data) {
                Log.log("Car position at the start: " + p);
            }
            justStarted = false;
        }

        // process all positions
        world.handleCarPositions(data);

        // if it's the first time call which inits car positions, we shouldn't do any actions yet
        if (raceInProgress) {
            // call main method in the child class to switch lanes, apply throttle, and turbo
            Move move = processCarPositions();
            processMove(move);
        }

        // make the data available to the chart renderer
        addDataToCharts();
    }

    private boolean moveAlternate;
    private int turboSafetyCountdown;

    private void processMove(Move move) {
        // this method prioritizes what to do -- send throttle, switch lanes, or apply turbo
        //
        // initially I was thinking of always giving lane switch first priority, but then
        // I decided to give it last and here is why :)
        //
        // here are some basic assumptions:
        //
        //   1. throttle
        //      * we need to be careful with it. a few wrong moves and we are off the track
        //      * good thing about throttle is that it can be the same for consecutive ticks, so we
        //        don't need to send it again if it's the same, and we can use this tick for turbo and switch
        //
        //   2. turbo
        //      * use it once and we are done with it util the next turbo comes
        //      * sometimes we can be stuck with turbo (e.g. we use it and nothing happens). we want to intelligently
        //        retry turbo in this case, without being stuck at trying turbo every tick!
        //
        //   3. switch
        //      * obviously, we should send it when we want to switch
        //      * the key observation is that we may want to revisit and overwrite our decision to change lanes!
        //        let's say there are 5 pieces before changing lanes. we originally decided that we should use the
        //        shortest line, but all the sudden we are approaching a slow opponent. we should be able to
        //        change our previous opinion and switch lanes! so multiple switch lane messages are possible of course
        //      * notes:
        //         if we sent "Left", we can't overwrite to get back to the current lane. we can only overwrite
        //         it with "Right". and vice versa
        //
        //
        // that give us the basic strategy which seems to work well
        //  - apply turbo if we are accelerating
        //  - give priority to throttle
        //  - when throttle is the same as on previous tick (we hope there will be plenty of points like this),
        //    alternate between turbo and switch lane

        boolean applied;

        // apply turbo if we are accelerating
        if (move.getThrottle() > getWorld().getMyCarData().getLastThrottleSent()) {
            applied = applyTurbo(move);
            if (applied) {
                return;
            }
        }

        // apply throttle
        applied = applyThrottle(move);
        if (applied) {
            return;
        }

        // alternate between turbo and lane changes
        if (moveAlternate) {
            applied = applyLaneChange(move);
            if (!applied) {
                applied = applyTurbo(move);
            }
        } else {
            applied = applyTurbo(move);
            if (!applied) {
                applied = applyLaneChange(move);
            }
        }
        moveAlternate = !moveAlternate;

        if (applied) {
            // applied change lanes or turbo
            return;
        }

        // if nothing has been applied still, let's force throttle
        move.setForceThrottleToBeApplied(true);
        applyThrottle(move);

        // decrement the counter
        turboSafetyCountdown--;
    }

    private boolean applyThrottle(Move move) {
        double prevThrottle = getWorld().getMyCarData().getLastThrottleSent();
        double throttle = move.getThrottle();
        if ((Math.abs(prevThrottle - throttle) > CarDataUtils.EPS && throttle > -CarDataUtils.EPS) || move.isForceThrottleToBeApplied()) {
            getMessageSender().sendMessageWithTick(new Throttle(throttle), getWorld().getCurrentTick());
            getWorld().getMyCarData().setLastThrottleSent(throttle);
            return true;
        }
        return false;
    }

    private boolean applyLaneChange(Move move) {
        int pIdx = getWorld().getMyCarData().getPos().piecePosition.pieceIndex;
        boolean hasSwitch = getWorld().getRace().track.pieces[pIdx].hasSwitch;
        if (hasSwitch) {
            // if we are in a switch, let's not apply any lane changes for god's sake
            return false;
        }

        int prevLaneSent = getWorld().getMyCarData().getLastSwitchLaneSent();
        int lane = move.getLaneIdx();
        if (lane != prevLaneSent && lane != Integer.MIN_VALUE && lane != getWorld().getMyCarData().getPos().piecePosition.lane.endLaneIndex) {
            CarPosition myPos = getWorld().getMyCarData().getPos();
            Log.log("Scheduling a lane change: " + myPos.piecePosition.lane.endLaneIndex + " -> " + lane);
            getMessageSender().sendMessageWithTick(new SwitchLane(myPos.piecePosition.lane.endLaneIndex, lane), getWorld().getCurrentTick());
            getWorld().getMyCarData().setLastSwitchLaneSent(lane);
            return true;
        }
        return false;
    }

    private boolean applyTurbo(Move move) {
        if (move.isTurboEnabled() && turboSafetyCountdown <= 0) {
            Log.log("Enabling turbo for our car " + getWorld().getMyCar().name);
            getMessageSender().sendMessageWithTick(new Turbo("Let's go!"), getWorld().getCurrentTick());
            turboSafetyCountdown = 20; // don't attempt to send turbo within the next 20 ticks
            return true;
        } else {
            return false;
        }
    }

    private void addDataToCharts() {
        CarPosition myPos = getWorld().getMyCarData().getPos();
        if (chartRenderer != null && getWorld().getMyCarData().getPrevPos() != null) {
            CarData data = getWorld().getMyCarData();
            Track.Piece piece = getWorld().getRace().track.pieces[myPos.piecePosition.pieceIndex];
            double pRadius = piece.radius != null ? piece.radius : 0.0;
            double pAngle = piece.angle != null ? piece.angle : 0.0;
            chartRenderer.addData(
                myPos.piecePosition.lap,
                data.getPosInBaseLane(true), // this is for charts only, so approximate is fine
                data.getSpeed(true), // this is for charts only, so approximate is fine
                data.getAngle(),
                pRadius * Math.signum(pAngle),
                data.getLastThrottleSent()
            );
        }
    }

    public final void handleCarCrash(Crash whoCrashed) {
        if (whoCrashed.equals(getWorld().getMyCar())) {
            Log.error("Our car " + getWorld().getMyCar().name + " crashed at tick " + getWorld().getCurrentTick() + ": " + getWorld().getMyCarData().getPosInBaseLane(true)); // this is for logging only, so approximate is fine
        }

        getWorld().getCarData(whoCrashed).markAsCrashed();
        processCarCrash(whoCrashed);
    }

    public final void handleCarDnf(Dnf dnf) {
        Log.error("Car " + dnf.getCar().name + " was disqualified at " + getWorld().getCurrentTick() + ": " + getWorld().getCarData(dnf.getCar()).getPosInBaseLane(true)); // this is for logging only, so approximate is fine

        getWorld().getCarData(dnf.getCar()).markAsCrashed();
    }

    public final void handleCarSpawn(Spawn whoSpawned) {
        getWorld().getCarData(whoSpawned).markAsSpawned();
        processCarSpawn(whoSpawned);
    }

    public final void handleTurboAvailable(TurboAvailable turboAvailable) {
        // turboAvailable message received between crash and spawn should by ignored (confirmed by organizers)
        if (!getWorld().getMyCarData().isCrashed()) {
            Log.log("Turbo is enabled on the track: " + turboAvailable);
            getWorld().getMyCarData().setTurboAvailable(turboAvailable);
        } else {
            Log.log("Turbo is enabled on the track: " + turboAvailable + ", but we are currently crashed so we don't get to use turbo");
        }
    }

    public void handleTurboStart(TurboStart turboStart) {
        if (turboStart.equals(getWorld().getMyCar())) {
            Log.log("Our car " + getWorld().getMyCar().name + " successfully enabled turbo");
        }
        getWorld().getCarData(turboStart).markTurboStart();
    }

    public void handleTurboEnd(TurboEnd turboEnd) {
        if (turboEnd.equals(getWorld().getMyCar())) {
            Log.log("Our car " + getWorld().getMyCar().name + " finished using turbo");
        }
        getWorld().getCarData(turboEnd).markTurboEnd();
    }

    public void handleCarFinish(Finish data) {
        getWorld().getCarData(data).markAsFinished();
    }

    public void startRace() {
        // this gets called once for the qualifier, and once for the race
        justStarted = true;
        raceInProgress = true;
        world.startRace();
        // since it's a start, send full throttle
        Move move = new Move();
        move.setThrottle(1.0);
        processMove(move);
    }

    public void endRace() {
        // this gets called once for the qualifier, and once for the race
        raceInProgress = false;
    }

    protected abstract Move processCarPositions();

    protected abstract void processCarCrash(Crash whoCrashed);

    protected abstract void processCarSpawn(Spawn whoSpawned);

}
