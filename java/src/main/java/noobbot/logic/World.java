package noobbot.logic;

import noobbot.io.Log;
import noobbot.logic.physics.WorldPhysics;
import noobbot.model.*;

import java.util.HashMap;
import java.util.Map;

public class World {

    // Current race type
    public enum RaceType {
        QUALIFIER,
        RACE,
        TESTING_SESSION
    }

    // Contains description of the track, lanes, and cars
    private Race race;

    // Our car
    private Car.Id myCar;
    private int myCarId = -1;

    // Current race type
    private RaceType currentRaceType;

    // All cars in the race
    private Map<Car.Id, Integer> carIds = new HashMap<>();
    private CarData[] carData;

    // Game ID and game tick
    private String gameId;
    private int currentTick = -1;

    // Pre-calculated sum of distances
    double[] sumDistInBaseLaneByPiece;

    // An instance of physics which will be set from the outside
    private WorldPhysics physics;

    public WorldPhysics getPhysics() {
        return physics;
    }

    public void setPhysics(WorldPhysics physics) {
        this.physics = physics;
    }

    public void init(GameInit gameInit) {
        // Init race
        setRace(gameInit.race);

        // Init approximate lengths for switches
        gameInit.race.track.initSwitchLengths(this);

        // Pre-calculate sum of distances (we'll need the precomputed value in CarDataUtils)
        sumDistInBaseLaneByPiece = new double[getRace().track.pieces.length];
        double result = 0.0;
        for (int i = 0; i < getRace().track.pieces.length; i++) {
            result += getRace().track.pieces[i].getSegmentLength( getRace().track.getBaseLane() );
            sumDistInBaseLaneByPiece[i] = result;
        }

        // Create mapping for all cars, so we can quickly look up IDs of each car
        // Note: this will be invoked for the actual race too
        for (Car c : gameInit.race.cars) {
            if (!carIds.containsKey(c.id)) {
                carIds.put(c.id, carIds.size());
            }
        }

        // Create new car data for each car
        carData = new CarData[carIds.size()];
        for (int i = 0; i < carData.length; i++) {
            carData[i] = new CarData(this);
            // mark them as crashed to get rid of disqualified ones
            carData[i].markAsCrashed();
        }
        // spawn cars which are alive
        for (Car c : gameInit.race.cars) {
            getCarData(c.id).markAsSpawned();
        }
    }

    public void setMyCar(Car.Id myCar) {
        this.myCar = myCar;
    }

    public Car.Id getMyCar() {
        return myCar;
    }

    public int getMyCarID() {
        if (myCarId == -1) {
            myCarId = carIds.get(getMyCar());
        }
        return myCarId;
    }

    public int getCarID(Car.Id carId) {
        return carIds.get(carId);
    }

    public CarData getCarData(int carId) {
        return carData[carId];
    }

    public CarData getCarData(Car.Id carId) {
        return getCarData(carIds.get(carId));
    }

    public CarData getMyCarData() {
        return getCarData(getMyCarID());
    }

    public CarData[] getCarDataAsArray() {
        return carData;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public void handleCarPositions(CarPosition[] data) {
        for (CarPosition pos : data) {
            int id = carIds.get(pos.id);
            carData[id].processCarPosition(pos);
        }
    }

    public void startRace() {
        // change the race type
        if (this.race.raceSession.quickRace) {
            this.currentRaceType = RaceType.TESTING_SESSION;
        } else if (this.currentRaceType == RaceType.QUALIFIER) {
            this.currentRaceType = RaceType.RACE;
        } else {
            this.currentRaceType = RaceType.QUALIFIER;
        }

        // second, re-init all the cars
        Log.log("The race is starting");
    }

    public RaceType getCurrentRaceType() {
        return currentRaceType;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        if (gameId != null) {
            this.gameId = gameId;
        }
    }

    public int getCurrentTick() {
        return currentTick;
    }

    public void setCurrentTick(Integer currentTick) {
        if (currentTick != null) {
            this.currentTick = currentTick;
            Log.setCurrentTick(currentTick);
        }
    }

    public double getSumDistInBaseLaneByPiece(int pieceIdx) {
        return sumDistInBaseLaneByPiece[pieceIdx];
    }

}
