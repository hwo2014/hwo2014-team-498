package noobbot.logic.physics;

import noobbot.io.Log;
import noobbot.logic.RadiusProvider;
import noobbot.logic.World;
import noobbot.model.Car;
import noobbot.model.Track;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WorldPhysics {
    private final World world;

    private double criticalAngle = 59.75;

    private double aphi1 = -0.1;
    private double aphiv = -0.00125;
    private double cv = -0.3;
    private double cvr = 0.53033;
    private double lambda = 0.020203;
    private double gamma = 0.202027;

    // related to logging
    private double[] lastReportedParams;

    private final ArrayList<Sample> samples = new ArrayList<>();
    // false until we set a and b, at which point we're assuming c1 and c2 can be correctly calculated
    private boolean isTrustable = false;
    // our assumption of v^2/r when we start slipping for learning (i.e. when isTrustable=false)
    private double v2rSlipStartForLearning = 0.1;

    private ArrayList<SwitchRadiusProvider> switchRadiusProviders = new ArrayList<>();

    public WorldPhysics(World world, String threadSuffix) {
        this.world = world;

        Thread calculator = new Thread("physics" + (threadSuffix == null ? "" : ("-" + threadSuffix))) {
            @Override
            public void run() {
                try {
                    while (!Thread.currentThread().isInterrupted()) {
                        synchronized (samples) {
                            samples.wait();
                        }
                        updateCoeffs();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        calculator.setDaemon(true);
        calculator.start();
    }

    public void setInitialParams(double aphi1, double aphiv, double cv, double cvr, double lambda, double gamma) {
        this.aphi1 = aphi1;
        this.aphiv = aphiv;
        this.cv = cv;
        this.cvr = cvr;
        this.lambda = lambda;
        this.gamma = gamma;
    }

    public void setCriticalAngle(double newCriticalAngle) {
        // TODO: refactor this piece in a smarter way (e.g. different critical angle on different segments),
        // TODO: make sure we don't set criticalAngle to Infinity
        // or probably just remove this logic completely

//        Log.error("Adjusting critical angle: " + newCriticalAngle);
//        criticalAngle = newCriticalAngle;
    }

    public double getCriticalAngle() {
        return criticalAngle;
    }

    public boolean isTrustable() {
        return isTrustable;
    }

    public double getV2rSlipStartForLearning() {
        return v2rSlipStartForLearning;
    }

    public void setV2rSlipStartForLearning(double v2rSlipStartForLearning) {
        this.v2rSlipStartForLearning = Math.max(this.v2rSlipStartForLearning, v2rSlipStartForLearning);
    }

    /**
     * Calculates the distance needed to change speed from v0 to v with a constant throttle.
     */
    public double getSpeedChangeDistance(double v0, double v, double throttle) {
        double gt = gamma * throttle;
        return (v0 - v) / lambda + gt / (lambda * lambda) * Math.log((lambda * v0 - gt) / (lambda * v) - gt);
    }

    public double getStaticSpeedForAngle(double angle, double radius) {
        double a = cvr / Math.sqrt(Math.abs(radius));
        double b = aphiv * angle + cv;
        double d = b * b - 4 * a;
        if (d < 0) {
            return 0;
        }
        return (-b + Math.sqrt(d)) / (2 * a);
    }

    /**
     * Returns a maximum constant speed which is safe to travel on a given radius.
     */
    public double getMaxSafeSpeed(double r) {
        if (isTrustable) {
            return getStaticSpeedForAngle(criticalAngle, r);
        }
        return getMaxNoSlipSpeed(r);
    }

    /**
     * Returns a maximum speed when there is no slip on a given radius.
     */
    public double getMaxNoSlipSpeed(double r) {
        if (isTrustable) {
            return getStaticSpeedForAngle(0, r);
        }
        return Math.sqrt(v2rSlipStartForLearning * Math.sqrt(r));
    }


    /**
     * Returns a throttle which has to be set to move with a constant speed v.
     */
    public double getConstantSpeedThrottle(double v) {
        return v * lambda / gamma;
    }

    /**
     * Returns a speed after applying throttle for some time
     *
     * @param v0       initial speed
     * @param throttle effective throttle (accounting for turboFactor)
     * @param ticks    number of ticks after which we want to know the new speed
     */
    public double getSpeedAfterThrottle(double v0, double throttle, double ticks) {
        double t1 = gamma * throttle / lambda;
        return (v0 - t1) * Math.exp(-lambda * ticks) + t1;
    }

    /*
     * This method is magic. Don't try to understand it.
     */
    private void updateCoeffs() {
        ArrayList<double[]> abSamples = new ArrayList<>();
        final ArrayList<Sample> samples;
        synchronized (this.samples) {
            samples = new ArrayList<>(this.samples);
        }
        for (int i = samples.size() - 1; i >= 0 && abSamples.size() < 10; i--) {
            Sample sample = samples.get(i);
//            double c = c1 * sample.v * sample.v / Math.abs(sample.r) + c2;
            if (/*c < 0*/ sample.isGood && Math.abs(sample.r1) > 500 && sample.phi1 != 0) { // TODO: tune limit, better use v^2/r
                abSamples.add(new double[]{sample.phi1, sample.phi * sample.v, sample.phi2});
            }
        }

        RealMatrix theta = leastSquares(abSamples);
        if (theta != null) {
            double error = getMaxError(abSamples, theta);
            if (error > 1e-5) {
                Log.error("Error is too high for aphi* samples, ignoring the result");
            } else {
                aphi1 = theta.getEntry(0, 0);
                aphiv = theta.getEntry(1, 0);
                ArrayList<double[]> cSamples = new ArrayList<>();
                for (int i = samples.size() - 1; i >= 0 && cSamples.size() < 10; i--) {
                    Sample sample = samples.get(i);
                    double c = (sample.phi2 - aphi1 * sample.phi1 - aphiv * sample.phi * sample.v)
                            * Math.signum(sample.r1);
                    if (sample.isGood && c > 0.1 && Math.abs(sample.r1) < 500) {
                        cSamples.add(new double[]{sample.v, sample.v * sample.v / Math.sqrt(Math.abs(sample.r1)), c});
                    }
                }
                theta = leastSquares(cSamples);
                if (theta != null) {
                    error = getMaxError(cSamples, theta);
                    if (error > 1e-5) {
                        Log.error("Error is too high for cv* samples, ignoring the result");
                    } else {
                        cv = theta.getEntry(0, 0);
                        cvr = theta.getEntry(1, 0);
                        if (!isTrustable) {
                            isTrustable = true;
                            Log.log("Finished learning physics, predictions are considered reliable");
                            logParams();
                        }
                    }
                }
            }
        }

        // TODO: stop calculating these after we got enough data (don't need to stop if we use online algorithm)
        ArrayList<double[]> lambdaSamples = new ArrayList<>();
        for (int i = samples.size() - 1; i >= 0; i--) {
            Sample sample = samples.get(i);
            if (sample.isGood && sample.throttle >= 0) {
                // otherwise it's a bot which throttle we don't know, so don't use these samples
                lambdaSamples.add(new double[]{sample.v, sample.throttle, sample.v + sample.a});
            }
            if (lambdaSamples.size() > 5) {
                break;
            }
        }

        theta = leastSquares(lambdaSamples);
        if (theta != null) {
            double error = getMaxError(lambdaSamples, theta);
            if (error > 1e-5) {
                Log.error("Error is too high for speed samples, ignoring the result");
            } else {
                double elambda = theta.getEntry(0, 0);
                double x1 = theta.getEntry(1, 0);
                lambda = -Math.log(elambda);
                gamma = lambda * x1 / (1 - elambda);
            }
        }

        boolean report = false;
        double[] newParams = new double[]{aphi1, aphiv, cv, cvr, lambda, gamma};
        if (lastReportedParams == null) {
            report = true;
        } else {
            for (int i = 0; i < newParams.length; i++) {
                double p = lastReportedParams[i];
                double q = newParams[i];
                if (Math.abs((p - q) / p) > 0.1) {
                    report = true;
                }
            }
        }
        lastReportedParams = newParams;
        if (report) {
            logParams();
        }

        if (isTrustable) {
            HashMap<SwitchRadiusProvider, ArrayList<double[]>> switchSamples = new HashMap<>();
            for (Sample s : samples) {
                if (s.r1 != s.r2) { // TODO: need to clean this, bad samples may be used
                    double c = (s.phi2 - aphi1 * s.phi1 - aphiv * s.phi * s.v) * Math.signum(s.angle);
                    if (c < 0.01) {
                        // Can reliably predict only in slip zones
                        continue;
                    }
                    double r = cvr * s.v * s.v / (c - cv * s.v);
                    r *= r;
                    SwitchRadiusProvider radiusProvider =
                            getRadiusProvider(Math.abs(s.r1), Math.abs(s.r2), Math.abs(s.angle));
                    if (!switchSamples.containsKey(radiusProvider)) {
                        switchSamples.put(radiusProvider, new ArrayList<>());
                    }
                    // otherwise it's a bot which throttle we don't know, so don't use these samples
                    double d = s.inPieceDistance;
                    switchSamples.get(radiusProvider).add(new double[]{d * d, d, 1.0, r});
                }
            }

            // TODO: do we need to get rid of bad samples here?

            for (Map.Entry<SwitchRadiusProvider, ArrayList<double[]>> entry : switchSamples.entrySet()) {
                theta = leastSquares(entry.getValue());
                if (theta != null) {
                    double rv2 = theta.getEntry(0, 0);
                    double rv1 = theta.getEntry(1, 0);
                    double rv0 = theta.getEntry(2, 0);
                    entry.getKey().update(rv2, rv1, rv0);
                }
            }
        }
    }

    public void logParams() {
        Log.log(String.format("aphi1=%.6f aphiv=%.6f cv=%.6f cvr=%.6f lambda=%.6f gamma=%.6f",
                aphi1, aphiv, cv, cvr, lambda, gamma));
    }

    private RealMatrix[] getXyMatrices(ArrayList<double[]> samples) {
        RealMatrix x = new Array2DRowRealMatrix(samples.size(), samples.get(0).length - 1);
        RealMatrix y = new Array2DRowRealMatrix(samples.size(), 1);
        for (int i = 0; i < samples.size(); i++) {
            double[] sample = samples.get(i);
            for (int j = 0; j < sample.length - 1; j++) {
                x.setEntry(i, j, sample[j]);
            }
            y.setEntry(i, 0, sample[sample.length - 1]);
        }
        return new RealMatrix[]{x, y};
    }

    private RealMatrix leastSquares(ArrayList<double[]> samples) {
        if (samples.size() >= 5) {
            RealMatrix[] xy = getXyMatrices(samples);
            RealMatrix x = xy[0];
            RealMatrix y = xy[1];
            RealMatrix x1 = x.transpose();
            try {
                RealMatrix pinv = new LUDecomposition(x1.multiply(x)).getSolver().getInverse();
                return pinv.multiply(x1.multiply(y));
            } catch (Exception ignored) {
            }
        }
        return null;
    }

    private double getMaxError(ArrayList<double[]> samples, RealMatrix theta) {
        RealMatrix[] xy = getXyMatrices(samples);
        RealMatrix x = xy[0];
        RealMatrix y = xy[1];
        RealMatrix errors = x.multiply(theta).subtract(y);
        double error = 0;
        for (double e : errors.getColumn(0)) {
            error = Math.max(error, Math.abs(e));
        }
        return error;
    }

    /**
     * Time after which angle oscillation with initial amplitude A will have amplitude A * dampingFactor.
     */
    public double getAngleDampingTime(double dampingFactor) {
        return Math.log(dampingFactor) * 2 / aphi1;
    }

    public Prediction getPrediction(double[] segmentLengths, RadiusProvider[] segmentRadii,
                                    double startAt, int startLap, double initialSpeed,
                                    double initialAngle, double initialAngularSpeed) {
        return new Prediction(this, segmentLengths, segmentRadii,
                startAt, startLap, initialSpeed, initialAngle, initialAngularSpeed);
    }

    public double getAngularAcceleration(double r, double speed, double angle, double angularSpeed) {
        double c = Math.max(0.0, cvr * speed * speed / Math.sqrt(Math.abs(r)) + cv * speed)
                * Math.signum(r);
        return aphi1 * angularSpeed + aphiv * angle * speed + c;
    }

    public void dumpSamples(PrintWriter writer) {
        for (Sample s : samples) {
            writer.println(String.format("%d\t%d\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f",
                    s.tick,
                    s.isGood ? 1 : 0,
                    s.r1 != Double.POSITIVE_INFINITY ? s.r1 : 1e10,
                    s.r2 != Double.POSITIVE_INFINITY ? s.r2 : 1e10,
                    s.angle,
                    s.throttle,
                    s.v,
                    s.a,
                    s.phi,
                    s.phi1,
                    s.phi2,
                    s.inPieceDistance));
        }
        writer.flush();
    }

    public void addSample(Sample sample) {
        synchronized (samples) {
            samples.add(
                    sample);
            samples.notify();
        }
        // TODO: move to logging
//                    Log.log("adding sample: "
//                            + Arrays.asList(tick, radius, throttle, speed, accel, angle, angleSpeed, angleAccel));
    }

    public double getThrottleToChangeSpeedInOneTick(double v0, double v1) {
        return (v1 - v0 * Math.exp(-lambda)) / (1 - Math.exp(-lambda)) * lambda / gamma;
    }

    public RadiusProvider getRadiusProvider(Track.Piece piece, int laneStartIdx, int laneEndIdx) {
        Track.Lane lane1 = world.getRace().track.getLaneByIndex(laneStartIdx);
        Track.Lane lane2 = world.getRace().track.getLaneByIndex(laneEndIdx);
        double r1 = piece.getLaneRadius(lane1);
        double r2 = piece.getLaneRadius(lane2);
        double radius = Math.min(r1, r2) * (piece.angle != null ? Math.signum(piece.angle) : 1.0);
        if (piece.angle == null || r1 == r2) {
            return new StaticRadiusProvider(radius);
        }
        double angle = piece.angle;
        SwitchRadiusProvider provider = getRadiusProvider(r1, r2, angle);
        if (angle > 0) {
            return provider;
        }
        return new RadiusProvider() {
            @Override
            public double getRadius(double inPieceDistance) {
                return -provider.getRadius(inPieceDistance);
            }

            @Override
            public boolean isRadiusKnown() {
                return provider.isRadiusKnown();
            }
        };
    }

    private SwitchRadiusProvider getRadiusProvider(double r1, double r2, double angle) {
        for (SwitchRadiusProvider provider : switchRadiusProviders) {
            if (Double.compare(r1, provider.r1) == 0 && Double.compare(r2, provider.r2) == 0
                    && Double.compare(Math.abs(angle), provider.angle) == 0) {
                return provider;
            }
        }
        SwitchRadiusProvider provider = new SwitchRadiusProvider(r1, r2, Math.abs(angle));
        switchRadiusProviders.add(provider);
        return provider;
    }

    private static class SwitchRadiusProvider implements RadiusProvider {
        final double r1;
        final double r2;
        final double angle;

        double rs2 = Double.NaN;
        double rs1;
        double rs0;

        private SwitchRadiusProvider(double r1, double r2, double angle) {
            this.r1 = r1;
            this.r2 = r2;
            this.angle = angle;
        }

        private void update(double rs2, double rs1, double rs0) {
            if (!isRadiusKnown() || Math.abs(rs2 - this.rs2) > 0.001 || Math.abs(rs1 - this.rs1) > 0.01
                    || Math.abs(rs0 - this.rs0) > 0.1) {
                Log.log("Learned curvature of switch: " + r1 + "-" + r2 + ", " + angle + ": " +
                        String.format("%.6f %.6f %.6f", rs2, rs1, rs0));
            }
            this.rs2 = rs2;
            this.rs1 = rs1;
            this.rs0 = rs0;
        }

        @Override
        public double getRadius(double inPieceDistance) {
            if (isRadiusKnown()) {
                return rs2 * inPieceDistance * inPieceDistance + rs1 * inPieceDistance + rs0;
            }
            return Math.min(r1, r2);
        }

        @Override
        public boolean isRadiusKnown() {
            return Double.isFinite(rs2);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SwitchRadiusProvider that = (SwitchRadiusProvider) o;

            if (Double.compare(that.angle, angle) != 0) return false;
            if (Double.compare(that.r1, r1) != 0) return false;
            //noinspection RedundantIfStatement
            if (Double.compare(that.r2, r2) != 0) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            temp = Double.doubleToLongBits(r1);
            result = (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(r2);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(angle);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }
    }

    private static class StaticRadiusProvider implements RadiusProvider {
        private final double radius;

        public StaticRadiusProvider(double radius) {
            this.radius = radius;
        }

        @Override
        public double getRadius(double ignored) {
            return radius;
        }

        @Override
        public boolean isRadiusKnown() {
            return true;
        }
    }

    public static class Sample {
        final int tick;
        final boolean isGood;
        final double r1;
        final double r2;
        final double angle;
        final double throttle;
        final double v;
        final double a;
        final double phi;
        final double phi1;
        final double phi2;
        final double inPieceDistance;
        final Car.Id carId;

        public Sample(int tick, boolean isGood, double r1, double r2, double angle, double throttle, double v, double a,
                      double phi, double phi1, double phi2, double inPieceDistance, Car.Id carId) {
            this.tick = tick;
            this.isGood = isGood;
            this.r1 = r1;
            this.r2 = r2;
            this.angle = angle;
            this.throttle = throttle;
            this.v = v;
            this.a = a;
            this.phi = phi;
            this.phi1 = phi1;
            this.phi2 = phi2;
            this.inPieceDistance = inPieceDistance;
            this.carId = carId;
        }

        @Override
        public String toString() {
            return "Sample{" +
                    "tick=" + tick +
                    ", isGood=" + isGood +
                    ", r1=" + r1 +
                    ", r2=" + r2 +
                    ", angle=" + angle +
                    ", throttle=" + throttle +
                    ", v=" + v +
                    ", a=" + a +
                    ", phi=" + phi +
                    ", phi1=" + phi1 +
                    ", phi2=" + phi2 +
                    ", inPieceDistance=" + inPieceDistance +
                    ", carId=" + carId +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Sample sample = (Sample) o;

            if (Double.compare(sample.a, a) != 0) return false;
            if (Double.compare(sample.angle, angle) != 0) return false;
            if (Double.compare(sample.inPieceDistance, inPieceDistance) != 0) return false;
            if (isGood != sample.isGood) return false;
            if (Double.compare(sample.phi, phi) != 0) return false;
            if (Double.compare(sample.phi1, phi1) != 0) return false;
            if (Double.compare(sample.phi2, phi2) != 0) return false;
            if (Double.compare(sample.r1, r1) != 0) return false;
            if (Double.compare(sample.r2, r2) != 0) return false;
            if (Double.compare(sample.throttle, throttle) != 0) return false;
            if (tick != sample.tick) return false;
            if (Double.compare(sample.v, v) != 0) return false;
            //noinspection RedundantIfStatement
            if (carId != null ? !carId.equals(sample.carId) : sample.carId != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            result = tick;
            result = 31 * result + (isGood ? 1 : 0);
            temp = Double.doubleToLongBits(r1);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(r2);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(angle);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(throttle);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(v);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(a);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(phi);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(phi1);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(phi2);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(inPieceDistance);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            result = 31 * result + (carId != null ? carId.hashCode() : 0);
            return result;
        }
    }
}
