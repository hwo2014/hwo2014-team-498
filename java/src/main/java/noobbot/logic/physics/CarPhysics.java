package noobbot.logic.physics;

import noobbot.io.Log;
import noobbot.logic.CarData;
import noobbot.logic.CarDataUtils;
import noobbot.logic.World;
import noobbot.model.Car;
import noobbot.model.CarPosition;
import noobbot.model.Track;

public class CarPhysics {

    private static final boolean ALLOW_APPROXIMATE_VALUES = true;

    private final World world;
    private final WorldPhysics calculator;
    private final Car.Id carId;

    private int prevPieceIndex = -1;
    private int skipTicks = 0;
    private double lastSpeed;
    private double lastAngle;
    private double lastAngleSpeed;

    private int samplesSkippedDueToDanger = 0;
    private int samplesSkippedTickLo = -1;
    private int samplesSkippedTickHi = -1;

    public CarPhysics(World world, WorldPhysics calculator, Car.Id carId) {
        this.world = world;
        this.calculator = calculator;
        this.carId = carId;
    }

    private boolean isSafeToCollectSample() {
        CarData data = world.getCarData(carId);

        if (data.getAcceleration(ALLOW_APPROXIMATE_VALUES) < -1.0) {
            // this car is stopping too fast, may be because of a collision
            return false;
        }

        for (Car opponent : world.getRace().cars) {
            // skip our own car
            if (carId.equals(opponent.id)) {
                continue;
            }

            // get opponent and see where he is
            CarData opponentData = world.getCarData(opponent.id);

            // if opponent is crashed (temporarily or permanently/dnf, let's skip him)
            if (!opponentData.isAlive()) {
                continue;
            }

            // get his piece
            int pieces = world.getRace().track.pieces.length;

            // get current distance
            // TODO: suboptimal performance
            double distance = Math.min(
                CarDataUtils.getDistanceTraveledBetweenPositions(world, data.getPos(), opponentData.getPos(), ALLOW_APPROXIMATE_VALUES),
                CarDataUtils.getDistanceTraveledBetweenPositions(world, opponentData.getPos(), data.getPos(), ALLOW_APPROXIMATE_VALUES)
            );

            // see where the opponent is
            boolean dangerousOpponent = false;
            if (opponentData.getPos().piecePosition.pieceIndex == data.getPos().piecePosition.pieceIndex) {
                // opponent is on the same piece. we consider him dangerous if we are on the same lane now, or we are merging into the same lane
                dangerousOpponent = (data.getPos().piecePosition.lane.startLaneIndex == opponentData.getPos().piecePosition.lane.startLaneIndex)
                    || (data.getPos().piecePosition.lane.endLaneIndex == opponentData.getPos().piecePosition.lane.endLaneIndex);
            } else if ((opponentData.getPos().piecePosition.pieceIndex + 1) % pieces == data.getPos().piecePosition.pieceIndex) {
                // opponent is on the previous piece, and he might merge into us
                dangerousOpponent = (opponentData.getPos().piecePosition.lane.endLaneIndex == data.getPos().piecePosition.lane.startLaneIndex)
                    || (opponentData.getPos().piecePosition.lane.endLaneIndex == data.getPos().piecePosition.lane.endLaneIndex);
            } else if ((data.getPos().piecePosition.pieceIndex + 1) % pieces == opponentData.getPos().piecePosition.pieceIndex) {
                // opponent is on the next piece and we might merge into him
                dangerousOpponent = (data.getPos().piecePosition.lane.endLaneIndex == opponentData.getPos().piecePosition.lane.startLaneIndex)
                    || (data.getPos().piecePosition.lane.endLaneIndex == opponentData.getPos().piecePosition.lane.endLaneIndex);
            }

            // if the opponent is dangerous, let's do additional checks
            if (dangerousOpponent) {
                double length = world.getRace().cars[world.getCarID(carId)].dimensions.length;
                // there is a precaution multiplier
                double safeDistance = 2.0 * length;
                if (opponentData.getAcceleration(ALLOW_APPROXIMATE_VALUES) < -1.0) {
                    // the opponent is stopping too fast, probably collided with us, let's increase the safe distance
                    safeDistance += 30.0; // probably too much, but better safe than sorry
                }
                if (distance < safeDistance) {
                    // Log.error(String.format("Ignoring sample because of dangerous opponent. Car %s, opponent %s, tick %d, distance %.2f, car length %.2f", carId.name, opponent.id.name, world.getCurrentTick(), distance, length));
                    return false;
                } else {
                    // Log.error(String.format("Opponent is possibly dangerous, but he is too far away. Car %s, opponent %s, tick %d, distance %.2f, car length %.2f", carId.name, opponent.id.name, world.getCurrentTick(), distance, length));
                }
            }

        }
        return true;
    }

    @SuppressWarnings("ConstantConditions")
    public void updateCarPosition() {
        CarData data = world.getCarData(carId);
        CarPosition pos = data.getPos();
        int tick = world.getCurrentTick();

        if (data != null) {
            if (pos.piecePosition.pieceIndex != prevPieceIndex) {
                // a workaround to skip 2 ticks after we moved to a next segment because of possible miscalculation
                skipTicks = 2;
            }
            // report sample only if all conditions are true
            boolean laterTicks = skipTicks-- <= 0;
            boolean sameLane = pos.piecePosition.lane.startLaneIndex == pos.piecePosition.lane.endLaneIndex;
            boolean hasSpeed = data.getSpeed(ALLOW_APPROXIMATE_VALUES) > CarDataUtils.EPS;
            boolean safeAround = isSafeToCollectSample();
            boolean reportSample = sameLane && laterTicks && hasSpeed && safeAround;

            CarPosition.PiecePosition prevPiecePos = data.getPrevPos().piecePosition;
            Track.Piece piece = world.getRace().track.pieces[prevPiecePos.pieceIndex];
            Track.Lane lane1 = world.getRace().track.getLaneByIndex(prevPiecePos.lane.startLaneIndex);
            Track.Lane lane2 = world.getRace().track.getLaneByIndex(prevPiecePos.lane.endLaneIndex);
            double turnAngle = piece.angle != null ? piece.angle : 0.0;
            double radius1 = piece.getLaneRadius(lane1) * (piece.angle != null ? Math.signum(piece.angle) : 1.0);
            double radius2 = piece.getLaneRadius(lane2) * (piece.angle != null ? Math.signum(piece.angle) : 1.0);

            if (data.isAlive()) {
                double throttle = world.getCarData(carId).getLastThrottleSent();
                WorldPhysics.Sample sample = new WorldPhysics.Sample(
                        tick - 1,
                        reportSample,
                        radius1,
                        radius2,
                        turnAngle,
                        throttle * data.getPrevTurboFactor(),
                        lastSpeed,
                        data.getAcceleration(ALLOW_APPROXIMATE_VALUES),
                        lastAngle,
                        lastAngleSpeed,
                        data.getAngleAcceleration(),
                        prevPiecePos.inPieceDistance,
                        carId
                );
                calculator.addSample(sample);
            }

            if (!safeAround) {
                samplesSkippedDueToDanger++;
                if (samplesSkippedTickLo == -1) {
                    samplesSkippedTickLo = world.getCurrentTick();
                }
                samplesSkippedTickHi = world.getCurrentTick();
            }

            lastSpeed = data.getSpeed(ALLOW_APPROXIMATE_VALUES); // TODO: all prev values are actually available in CarData
            lastAngle = data.getAngle();
            lastAngleSpeed = data.getAngleSpeed();
        }

        prevPieceIndex = pos.piecePosition.pieceIndex;

        if (tick % 100 == 0) {
            Log.log("Position for " + carId.name + ": " + world.getCarData(carId).getPosInBaseLane(true)); // this is for logging only, so approximate is fine
            if (samplesSkippedDueToDanger > 0) {
                Log.error("Samples skipped for car " + carId.name + " due to crash danger: " + samplesSkippedDueToDanger + ", somewhere between ticks [" + samplesSkippedTickLo + ", " + samplesSkippedTickHi + "]");
            }
            samplesSkippedDueToDanger = 0;
            samplesSkippedTickLo = -1;
            samplesSkippedTickHi = -1;
        }
    }

}
