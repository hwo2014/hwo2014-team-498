package noobbot.logic.physics;

import noobbot.logic.RadiusProvider;

public class Prediction {
    private final WorldPhysics physics;
    private final double[] segmentLengths;
    private final RadiusProvider[] segmentRadii;
    private final int segmentIndex;
    private final double segmentPos;
    private final int startLap;
    private final double speed;
    private final double angle;
    private final double angularSpeed;

    private Prediction(WorldPhysics physics, int startLap,
                       double[] segmentLengths, RadiusProvider[] segmentRadii,
                       int startSegmentIndex, double startSegmentPos, double initialSpeed,
                       double initialAngle, double initialAngularSpeed) {
        this.physics = physics;
        this.segmentLengths = segmentLengths;
        this.segmentRadii = segmentRadii;
        this.segmentIndex = startSegmentIndex;
        this.segmentPos = startSegmentPos;
        this.startLap = startLap;
        this.speed = initialSpeed;
        this.angle = initialAngle;
        this.angularSpeed = initialAngularSpeed;
    }

    Prediction(WorldPhysics physics, double[] segmentLengths, RadiusProvider[] segmentRadii,
               double startAt, int startLap, double initialSpeed, double initialAngle, double initialAngularSpeed) {
        this(physics, startLap, segmentLengths, segmentRadii,
                getStartSegmentIndex(segmentLengths, startAt), getStartSegmentRemainder(segmentLengths, startAt),
                initialSpeed, initialAngle, initialAngularSpeed);
    }

    private static int getStartSegmentIndex(double[] segmentLengths, double startAt) {
        int seg = 0;
        while (seg < segmentLengths.length && startAt >= segmentLengths[seg]) {
            startAt -= segmentLengths[seg];
            seg += 1;
        }
        return seg;
    }

    private static double getStartSegmentRemainder(double[] segmentLengths, double startAt) {
        int seg = 0;
        while (seg < segmentLengths.length && startAt >= segmentLengths[seg]) {
            startAt -= segmentLengths[seg];
            seg += 1;
        }
        return startAt;
    }

    /**
     * Advances the prediction by 1 tick moving with throttle={@code effectiveThrottle}.
     */
    public Prediction move(double effectiveThrottle) {
        final double newAngle;
        final double newAngularSpeed;
        final double radius;
        if (segmentIndex < segmentRadii.length) {
            radius = segmentRadii[segmentIndex].getRadius(segmentPos);
        } else {
            radius = Double.POSITIVE_INFINITY;
        }
        if (physics.isTrustable()) {
            double angularAcceleration = physics.getAngularAcceleration(radius, speed, angle, angularSpeed);
            newAngularSpeed = angularSpeed + angularAcceleration;
            newAngle = angle + newAngularSpeed;
        } else {
            // using learning algorithm
            if (speed * speed / Math.sqrt(Math.abs(radius)) <= physics.getV2rSlipStartForLearning()) {
                newAngle = 0.0;
            } else {
                // Return very large angle, since we don't want to raise speed in this case
                newAngle = 1e10;
            }
            newAngularSpeed = 0.0;
        }
        double newSpeed = physics.getSpeedAfterThrottle(speed, effectiveThrottle, 1.0);
        double newSegmentPos = segmentPos + newSpeed;
        int newSegmentIndex = segmentIndex;
        while (newSegmentIndex < segmentLengths.length && newSegmentPos >= segmentLengths[newSegmentIndex]) {
            newSegmentPos -= segmentLengths[newSegmentIndex];
            newSegmentIndex += 1;
        }
        if (newSegmentIndex >= segmentLengths.length) {
            // out of bounds, returning zeros
            return new Prediction(physics, startLap, segmentLengths, segmentRadii,
                    newSegmentIndex, 0, 0.0, 0.0, 0.0);
        }
        return new Prediction(physics, startLap, segmentLengths, segmentRadii,
                newSegmentIndex, newSegmentPos, newSpeed, newAngle, newAngularSpeed);
    }

    public double getAngle() {
        return angle;
    }

    public double getAngularSpeed() {
        return angularSpeed;
    }

    public double getSpeed() {
        return speed;
    }

    public boolean isRadiusKnown() {
        return segmentIndex >= segmentRadii.length
                || segmentRadii[segmentIndex].isRadiusKnown();
    }

    public boolean hasReachedEnd() {
        return segmentIndex >= segmentLengths.length;
    }

    public int getStartLap() {
        return startLap;
    }

    public int getSegmentIndex() {
        return segmentIndex;
    }

    public double getSegmentPos() {
        return segmentPos;
    }
}
