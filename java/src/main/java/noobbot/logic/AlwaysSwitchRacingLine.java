package noobbot.logic;

import noobbot.model.CarPosition;
import noobbot.model.Track;

public class AlwaysSwitchRacingLine extends RacingLine {

    private int dxGlobal = -1;

    public AlwaysSwitchRacingLine(World world) {
        super(world);
    }

    @Override
    public int[] calcNextLanes(CarPosition pos, boolean overtake) {
        int pIdx = pos.piecePosition.pieceIndex;
        int startLaneIdx = pos.piecePosition.lane.startLaneIndex;
        int endLaneIdx = pos.piecePosition.lane.endLaneIndex;

        Track.Piece[] pieces = getWorld().getRace().track.pieces;
        int piecesCnt = pieces.length;

        Track.Lane[] lanes = getWorld().getRace().track.lanes;
        int lanesCnt = lanes.length;

        int lapsToMake = 2;

        int[] ansLanes = new int[lapsToMake * piecesCnt + 1];
        ansLanes[0] = startLaneIdx;
        ansLanes[1] = endLaneIdx;

        if (!(0 <= ansLanes[1] + dxGlobal && ansLanes[1] + dxGlobal < lanesCnt)) {
            // we are doing lane changes in cycles
            // e.g. for 3 lanes: 0 -> 1 -> 2 -> 1 -> 0 -> 1 -> 2 -> ...
            dxGlobal = -dxGlobal;
        }

        int dx = dxGlobal;
        for (int i = 1; i < ansLanes.length - 1; i++) {
            int currentPieceId = (pIdx + i) % piecesCnt;
            if (pieces[currentPieceId].hasSwitch) {

                // move one direction
                if (0 <= ansLanes[i] + dx && ansLanes[i] + dx < lanesCnt) {
                    ansLanes[i + 1] = ansLanes[i] + dx;
                } else {
                    // move another direction
                    dx = -dx;
                    if (0 <= ansLanes[i] + dx && ansLanes[i] + dx < lanesCnt) {
                        ansLanes[i + 1] = ansLanes[i] + dx;
                    } else {
                        throw new IllegalStateException("Always switch lanes algorithm doesn't really work");
                    }
                }

            } else {
                ansLanes[i + 1] = ansLanes[i];
            }

        }

        return ansLanes;
    }

}
