package noobbot.logic;

import noobbot.model.CarPosition;
import noobbot.model.Track;

import java.util.Arrays;

@SuppressWarnings("FieldCanBeLocal")
public class ShortestRacingLine extends RacingLine {

    private static boolean IS_TURNED_ON = true;

    public ShortestRacingLine(World world) {
        super(world);
    }

    @SuppressWarnings("SimplifiableConditionalExpression")
    @Override
    public int[] calcNextLanes(CarPosition pos, boolean overtake) {
        int pIdx = pos.piecePosition.pieceIndex;
        int startLaneIdx = pos.piecePosition.lane.startLaneIndex;
        int endLaneIdx = pos.piecePosition.lane.endLaneIndex;

        Track.Piece[] pieces = getWorld().getRace().track.pieces;
        int piecesCnt = pieces.length;

        Track.Lane[] lanes = getWorld().getRace().track.lanes;
        int lanesCnt = lanes.length;

        int lapsToMake = 2;

        // we want to make several whole laps and see where we will end up
        double[][] d = new double[lapsToMake * piecesCnt + 1][lanesCnt];
        int[][] from = new int[lapsToMake * piecesCnt + 1][lanesCnt];
        int[] ansLanes = new int[lapsToMake * piecesCnt + 1];

        for (int[] v : from) {
            Arrays.fill(v, -1);
        }
        for (double[] v : d) {
            Arrays.fill(v, Double.MAX_VALUE);
        }
        Arrays.fill(ansLanes, -1);

        int[] slowOpponentCnt = null;
        int[][] slowOpponents = null;

        // calculate distance to opponents
        int carsCnt = getWorld().getRace().cars.length;
        double[] distToOpponent = new double[carsCnt];
        double[] oppSpeed = new double[carsCnt];
        double maxSpeed = 0.0;
        for (int i = 0; i < carsCnt; i++) {
            CarData opponentData = getWorld().getCarDataAsArray()[i];
            distToOpponent[i] = CarDataUtils.getDistanceTraveledBetweenPositions(getWorld(), pos, opponentData.getPos(), true);
            oppSpeed[i] = opponentData.getSpeed(true);
            maxSpeed = Math.max(maxSpeed, oppSpeed[i]);
        }

        int carId = getWorld().getCarID(pos.id);
        CarData carData = getWorld().getCarData(pos.id);
        int carLap = carData.getPos().piecePosition.lap;

        double carL = getWorld().getRace().cars[carId].dimensions.length;
        double veryCloseDistance = 1.20 * carL;
        double lookAheadOpponentDistance = 8.0 * carL;

        // look ahead and see if there is any lap traffic ahead of us
        if (overtake) {

            slowOpponentCnt = new int[piecesCnt];
            slowOpponents = new int[piecesCnt][carsCnt];

            for (int i = 0; i < carsCnt; i++) {
                CarData opponentData = getWorld().getCarDataAsArray()[i];
                if (opponentData.isAlive() && !pos.id.equals(opponentData.getPos().id)) {
                    if (distToOpponent[i] < lookAheadOpponentDistance) {
                        // this opponent is ahead of us, so he is potentially dangerous
                        int opponentId = getWorld().getCarID(opponentData.getPos().id);
                        int opponentLap = opponentData.getPos().piecePosition.lap;

                        boolean lapTraffic = (carLap > opponentLap || (carLap == opponentLap && opponentData.getPos().piecePosition.pieceIndex < carData.getPos().piecePosition.pieceIndex));

                        boolean weAreMuchFaster = (getWorld().getPhysics() != null && getWorld().getPhysics().isTrustable()) ?
                            carData.wayFasterThan(opponentData) :
                            true;  // encourage to change lanes while we are learning physics (otherwise we might never learn)

                        boolean tooCloseAndSamePieceSameLane = (getWorld().getPhysics() != null && getWorld().getPhysics().isTrustable()) ?
                            carData.gotStuckBehind(opponentData, veryCloseDistance) :
                            true;  // encourage to change lanes while we are learning physics (otherwise we might never learn)

                        boolean shouldOvertake = lapTraffic || weAreMuchFaster || tooCloseAndSamePieceSameLane;

                        if (shouldOvertake) {
                            /*
                            if (carData.getPos().id.equals(getWorld().getMyCarData().getPos().id) && (getWorld().getPhysics() != null && getWorld().getPhysics().isTrustable())) {
                                Log.error(carData.getPos().id.name + " scheduling real overtake of " + opponentData.getPos().id.name + ": LT " + lapTraffic + ", MF " + weAreMuchFaster + ", TC " + tooCloseAndSamePieceSameLane);
                            }
                            */

                            // mark segments where opponent will likely be traveling as "expensive"
                            int cPieceIdx = opponentData.getPos().piecePosition.pieceIndex;

                            // first of all - go forward by lookahead distance
                            double distForward = 0.0;
                            boolean switchPassed = false;
                            int sTotal = piecesCnt;
                            while (distForward < lookAheadOpponentDistance && sTotal > 0) {
                                // add opponent to the list
                                slowOpponents[cPieceIdx][slowOpponentCnt[cPieceIdx]] = opponentId;
                                slowOpponentCnt[cPieceIdx]++;
                                distForward += pieces[cPieceIdx].getLength(endLaneIdx, endLaneIdx, true); // this is just an approximation

                                if (pieces[cPieceIdx].hasSwitch) {
                                    switchPassed = true;
                                }
                                cPieceIdx = (cPieceIdx + 1) % piecesCnt;
                                sTotal--;
                            }

                            // if we haven't face any switches yet, go even longer
                            while (!switchPassed && sTotal > 0) {
                                // add opponent to the list
                                slowOpponents[cPieceIdx][slowOpponentCnt[cPieceIdx]] = opponentId;
                                slowOpponentCnt[cPieceIdx]++;

                                if (pieces[cPieceIdx].hasSwitch) {
                                    switchPassed = true;
                                }
                                cPieceIdx = (cPieceIdx + 1) % piecesCnt;
                                sTotal--;
                            }
                        }
                    }
                }
            }

        }

        // we don't have much options in the beginning, when we are in the current piece
        // d[pieceIdx][laneIdx] -- we are standing in the beginning of piece [pieceIdx] in lane[laneIdx]
        // d[0][j] - points to the "current" piece, which is pIdx
        // d[1][j] - points to the "next" piece, which is (pIdx + 1)
        d[0][startLaneIdx] = 0.0;
        d[1][endLaneIdx] = CarDataUtils.getActualDistanceToPieceEnd(getWorld(), pos, true);
        from[1][endLaneIdx] = startLaneIdx;

        // now, let's do a DP
        boolean firstSwitch = true;
        for (int i = 1; i < d.length - 1; i++) {
            int currentPieceId = (pIdx + i) % piecesCnt;

            // if we already sent a lane switch message, we won't be able to go straight anymore on next switch
            int bannedDirection = -1;
            if (pieces[currentPieceId].hasSwitch) {
                if (firstSwitch) {
                    if (carData.getLastSwitchLaneSent() >= 0) {
                        // current lane
                        bannedDirection = endLaneIdx;
                    }
                    firstSwitch = false;
                }
            }

            for (int j = 0; j < lanesCnt; j++)
                if (d[i][j] < Double.MAX_VALUE)
                    for (int k = -1; k <= 1; k++)
                        if (0 <= j + k && j + k < lanesCnt) {
                            boolean canGo = ((k == 0) || (pieces[currentPieceId].hasSwitch)) && (j + k != bannedDirection);
                            if (!canGo) {
                                continue;
                            }

                            double multiplier = 1.0;
                            if (overtake) {

                                // loop through detected lap traffic opponents
                                int enemyCnt = 0;
                                double enemySpeed = Double.MAX_VALUE;
                                for (int opIdx = 0; opIdx < slowOpponentCnt[currentPieceId]; opIdx++) {
                                    int opponentIdx = slowOpponents[currentPieceId][opIdx];
                                    int[] opponentPredictedLanes = getWorld().getCarData(opponentIdx).getLastLaneForPiece();
                                    int opLane = opponentPredictedLanes[currentPieceId];
                                    int opLaneNext = opponentPredictedLanes[(currentPieceId + 1) % piecesCnt];

                                    if (opLaneNext >= 0) {
                                        // we know for sure where the guy will be
                                        if (opLaneNext == j + k) {
                                            // we will end on the same end lane
                                            enemyCnt++;
                                            enemySpeed = Math.min(enemySpeed, oppSpeed[opIdx]);
                                        }
                                    } else if (opLane >= 0 && opLaneNext < 0) {
                                        // we know roughly where the guy will be, so count him
                                        // this is a conservative assumption, that he will stay in the same lane
                                        if (Math.abs(opLane - (j + k)) == 0) {
                                            enemyCnt++;
                                            enemySpeed = Math.min(enemySpeed, oppSpeed[opIdx]);
                                        }
                                    }
                                }

                                if (enemyCnt > 0) {
                                    // add distance correction. closer opponents result in bigger multiplier
                                    multiplier += 1.0 + (1.0 - enemySpeed / maxSpeed);
                                }

                                /*
                                if (multiplier > 1.0) {
                                    Log.error(String.format("Traffic multiplier = %.2f, piece = %d, lane1 = %d, lane2 = %d", multiplier, currentPieceId, j, j + k));
                                }
                                */
                            }

                            double dist = pieces[currentPieceId].getLength(j, j + k, true) * multiplier;
                            if (d[i + 1][j + k] > d[i][j] + dist) {
                                d[i + 1][j + k] = d[i][j] + dist;
                                from[i + 1][j + k] = j;
                            }
                        }
        }

        int bestJ = -1;
        for (int j = 0; j < lanesCnt; j++)
            if (bestJ == -1 || d[d.length - 1][j] < d[d.length - 1][bestJ]) {
                bestJ = j;
            }

        if (d[d.length - 1][bestJ] == Double.MAX_VALUE) {
            throw new IllegalStateException("This is really unexpected, but we haven't really found a way around the track");
        }

        for (int i = d.length - 1; i >= 0; i--) {
            if (IS_TURNED_ON) {
                ansLanes[i] = bestJ;
            } else {
                ansLanes[i] = endLaneIdx;
            }
            bestJ = from[i][bestJ];
        }

        if (ansLanes[0] != startLaneIdx || ansLanes[1] != endLaneIdx) {
            throw new IllegalStateException("There is something wrong with DP");
        }

        return ansLanes;
    }

}
