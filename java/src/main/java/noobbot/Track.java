package noobbot;

public enum Track {

    FINLAND("keimola"),
    GERMANY("germany"),
    USA("usa"),
    FRANCE("france"),
    ELAEINTARHA("elaeintarha"),
    ITALY("imola"),
    ENGLAND("england"),
    JAPAN("suzuka"),
    PENTAG("pentag");

    private String name;

    private Track(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
