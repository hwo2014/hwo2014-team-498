package noobbot.stats;

public class Statistics {

    private long max = 0;
    private long sum = 0;
    private int count = 0;

    public void addValue(long v) {
        sum += v;
        count++;
        max = Math.max(max, v);
    }

    public long getMax() {
        return max;
    }

    public double getAvg() {
        return count > 0 ? sum / count : 0.0;
    }

    @Override
    public String toString() {
        return String.format("%d ms avg, %s ms max", Math.round(getAvg()), getMax());
    }
}
