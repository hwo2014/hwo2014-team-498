package noobbot;

import noobbot.io.MessageSender;
import noobbot.logic.PhysicsStrategy;
import noobbot.logic.Strategy;
import noobbot.logic.World;
import noobbot.model.Bot;
import noobbot.model.JoinRace;

import java.io.IOException;
import java.util.Random;

/**
 * This is class that we can use for the mass-testing of bots
 *
 * Here you can:
 *   - add multiple bots to a single game
 *
 *   ******************************************************************
 *   MAKE SURE TO POINT IT TO SPECIFIC SERVER, NOT TO THE LOAD BALANCER
 *
 *   Example of parameters:
 *     senna.helloworldopen.com 8091 "Team Saratov" 3Ad3PGsKxyVI5A
 *   ******************************************************************
 */
public class MainGroupTest extends BaseBot {

    // select number of bots here
    private static final int NUMBER_OF_BOTS = 2; //  more than 2 -> they will likely never learn physics

    // select a track here
    private static final Track TRACK = Track.USA;

    // password will be randomly generated
    private static final String PASSWORD = genRandomPassword();

    private static String genRandomPassword() {
        Random random = new Random();
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            buf.append((char) ('a' + random.nextInt(26)));
        }
        return buf.toString();
    }

    @SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
    private int id;

    public MainGroupTest(int id, String[] args) {
        this.id = id;
        this.args = args.clone();
        this.args[2] += " " + id; // adjust team name
    }

    public static void main(String... args) throws IOException, InterruptedException {
        Thread[] threads = new Thread[NUMBER_OF_BOTS];
        for (int i = 0; i < NUMBER_OF_BOTS; i++) {
            BaseBot bot = new MainGroupTest(i, args);
            threads[i] = new Thread(bot);
            threads[i].start();
            bot.waitUntilConnected();
        }
        for (int i = 0; i < NUMBER_OF_BOTS; i++) {
            threads[i].join();
        }
    }

    @Override
    public Strategy createStrategy(World world, MessageSender sender) {
//        Strategy strategy = new TrackLearningStrategy(world, sender);
        return new PhysicsStrategy(world, sender);
    }

    @Override
    protected void joinGame(Bot bot, MessageSender sender) {
        JoinRace join = new JoinRace(bot, NUMBER_OF_BOTS, TRACK.getName(), PASSWORD);
        sender.sendMessageNoTick(join);
    }

}