package noobbot.charts;

import noobbot.io.Log;
import noobbot.io.MessageUtils;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class ChartRenderer {

    private Map<Integer, List<Double>> seriesX = new LinkedHashMap<>();
    private Map<Integer, List<Double>> radiusY = new LinkedHashMap<>();
    private Map<Integer, List<Double>> speedY = new LinkedHashMap<>();
    private Map<Integer, List<Double>> angleY = new LinkedHashMap<>();
    private Map<Integer, List<Double>> throttleX = new LinkedHashMap<>();
    private Map<Integer, List<double[]>> turboX = new HashMap<>();

    private boolean opponent = false;

    public ChartRenderer() {
    }

    public void addData(int lap, double pos, double speed, double angle, double radius, double throttle) {
        addValue(seriesX, lap, pos);
        addValue(radiusY, lap, radius);
        addValue(speedY, lap, speed);
        addValue(angleY, lap, angle);
        addValue(throttleX, lap, throttle);
    }

    private void addValue(Map<Integer, List<Double>> map, int lap, double value) {
        List<Double> valuesY = map.get(lap);
        if (valuesY == null) {
            valuesY = new ArrayList<>();
            map.put(lap, valuesY);
        }
        valuesY.add(value);
    }

    public void setOpponent(boolean opponent) {
        this.opponent = opponent;
    }

    private List<Double> round(List<Double> list) {
        List<Double> result = new ArrayList<>();
        for (Double v : list) {
            result.add(Math.round(v * 1000.0) / 1000.0);
        }
        return result;
    }

    private List<double[]> roundL(List<double[]> list) {
        List<double[]> result = new ArrayList<>();
        if (list != null) {
            for (double[] a : list) {
                double[] aClone = a.clone();
                for (int i = 0; i < aClone.length; i++) {
                    aClone[i] = Math.round(aClone[i] * 1000.0) / 1000.0;
                }
                result.add(aClone);
            }
        }
        return result;
    }

    private String readFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        StringBuilder buf = new StringBuilder();
        String ls = System.getProperty("line.separator");

        String line;
        while ((line = reader.readLine()) != null) {
            buf.append(line);
            buf.append(ls);
        }

        return buf.toString();
    }

    private String readTemplate(String name) {
        try {
            return readFile("charts/templates/" + name + ".template");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void writeTo(String fileName, String carName, String trackId) {
        Log.log("Writing chart to " + fileName);

        // Read templates
        String lapT = readTemplate("lap");
        String trackDataT = readTemplate("trackData");

        // process all laps
        StringBuilder lapData = new StringBuilder();
        for (int lap : seriesX.keySet()) {
            List<Double> xList = round(seriesX.get(lap));

            // some laps usually has only few measurements (when we cross start/finish line), so they need to be discarded
            if (xList.size() < 5) {
                continue;
            }

            List<Double> radiusList = round(radiusY.get(lap));
            List<Double> speedList = round(makeSmooth(speedY.get(lap)));
            List<Double> angleList = round(angleY.get(lap));
            List<Double> throttleList = round(throttleX.get(lap));
            List<double[]> turboList = roundL(turboX.get(lap));

            String lapResult = lapT.replace("%%LAP%%", MessageUtils.encodeJson(lap))
                .replace("%%X%%", MessageUtils.encodeJson(xList))
                .replace("%%SPEED%%", MessageUtils.encodeJson(speedList))
                .replace("%%ANGLE%%", MessageUtils.encodeJson(angleList))
                .replace("%%THROTTLE%%", MessageUtils.encodeJson(throttleList))
                .replace("%%RADIUS%%", MessageUtils.encodeJson(radiusList))
                .replace("%%TURBO%%", MessageUtils.encodeJson(turboList));

            if (lapData.length() > 0) {
                lapData.append(",\n");
            }
            lapData.append(lapResult);
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date now = new Date();

        String result = trackDataT.replace("%%TEAM_NAME%%", MessageUtils.encodeJson(carName))
            .replace("%%TRACK_NAME%%", MessageUtils.encodeJson(trackId))
            .replace("%%LAP_DATA%%", lapData)
            .replace("%%IS_OPPONENT%%", MessageUtils.encodeJson(opponent))
            .replace("%%DATE_CREATED%%", MessageUtils.encodeJson(dateFormat.format(now)));

        try {
            PrintWriter out = new PrintWriter(new FileOutputStream(fileName));
            out.println(result);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private boolean isOutOfLine(double a1, double a2, double a3) {
        double d1 = Math.abs(a1 - a2);
        double d2 = Math.abs(a2 - a3);
        double d = Math.abs(a1 - a3);
        double ratio1 = Math.abs(d1 - d) / Math.max(Math.abs(a1), Math.abs(a3));
        double ratio2 = Math.abs(d2 - d) / Math.max(Math.abs(a1), Math.abs(a3));
        return ratio1 > 0.5 && ratio2 > 0.5;
    }

    private List<Double> makeSmooth(List<Double> list) {
        for (int i = 0; i < list.size() - 2; i++) {
            double a1 = list.get(i);
            double a2 = list.get(i + 1);
            double a3 = list.get(i + 2);
            if (isOutOfLine(a1, a2, a3)) {
                Log.error("Detected possibly bad speed on the chart: " + a1 + ", " + a2 + " [bad], " + a3 + ". Adjusting it.");
                list.set(i + 1, 0.5 * (a1 + a3));
            }
        }
        return list;
    }

    public void setTurbo(Map<Integer, List<double[]>> turboActivations) {
        this.turboX = turboActivations;
    }

}
