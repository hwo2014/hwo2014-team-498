package noobbot.debugging;

import noobbot.BaseBot;
import noobbot.Track;
import noobbot.io.MessageSender;
import noobbot.logic.Strategy;
import noobbot.logic.World;
import noobbot.model.Bot;
import noobbot.model.JoinRace;

import java.io.IOException;

public class Sampler extends BaseBot {

    // select a track here
    private static final Track TRACK = Track.USA;

    public static void main(String... args) throws IOException {
        new Sampler().run(args);
    }

    @Override
    public Strategy createStrategy(World world, MessageSender sender) {
        Strategy strategy = new SamplingStrategy(world, sender);
        strategy.enableCharts();
        return strategy;
    }

    @Override
    protected void joinGame(Bot bot, MessageSender sender) {
        JoinRace join = new JoinRace(bot, 1, TRACK.getName());
        sender.sendMessageNoTick(join);
    }
}
