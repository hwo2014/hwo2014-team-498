package noobbot.debugging;

import noobbot.io.MessageSender;
import noobbot.logic.Move;
import noobbot.logic.Strategy;
import noobbot.logic.World;
import noobbot.logic.physics.CarPhysics;
import noobbot.logic.physics.WorldPhysics;
import noobbot.model.Crash;
import noobbot.model.Spawn;
import org.apache.commons.math3.util.Pair;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class SamplingStrategy extends Strategy {

    private static final boolean ALLOW_APPROXIMATE_VALUES = true;

    final WorldPhysics physics;
    CarPhysics cf;

    public SamplingStrategy(World world, MessageSender messageSender) {
        super(world, messageSender);
        physics = new WorldPhysics(world, null);
    }

    public ThrottleProvider swingUp(double accel, double maxSpeed, ThrottleProvider andThen) {
        double v0 = world.getMyCarData().getSpeed(ALLOW_APPROXIMATE_VALUES);
        if (v0 > maxSpeed) {
            return andThen;
        } else {
            return () -> new Pair<>(
                    physics.getThrottleToChangeSpeedInOneTick(v0, v0 + accel),
                    swingUp(accel, maxSpeed, andThen));
        }
    }

    public ThrottleProvider swingDown(double accel, double minSpeed, ThrottleProvider andThen) {
        double v0 = world.getMyCarData().getSpeed(ALLOW_APPROXIMATE_VALUES);
        if (v0 < minSpeed) {
            return andThen;
        } else {
            return () -> new Pair<>(
                    physics.getThrottleToChangeSpeedInOneTick(v0, v0 + accel),
                    swingDown(accel, minSpeed, andThen));
        }
    }

    public ThrottleProvider swingBetween(double upAccel, double downAccel, double maxSpeed, double minSpeed) {
        return swingUp(upAccel, maxSpeed,
                swingDown(downAccel, minSpeed,
                        () -> swingBetween(upAccel, downAccel, maxSpeed, minSpeed).next()
                )
        );
    }

    ThrottleProvider throttleProvider;
    double lastLap = -1;

    @Override
    protected Move processCarPositions() {
//        double minSpeed = 8.3;
//        double maxSpeed = 9.8;
        double minSpeed = 8.1;
        double maxSpeed = 8.5;
        double accel = 0.1;
        if (cf == null) {
            cf = new CarPhysics(world, physics, world.getMyCar());
        }
        if (world.getMyCarData().getPos().piecePosition.lap > lastLap) {
            lastLap = world.getMyCarData().getPos().piecePosition.lap;
            throttleProvider = swingBetween(accel, -accel * (lastLap + 1), maxSpeed, minSpeed);
        }
        cf.updateCarPosition();
        double throttle;
        if (world.getMyCarData().getSpeed(ALLOW_APPROXIMATE_VALUES) < minSpeed - accel * 20) {
            throttle = 1.0;
        } else {
            Pair<Double, ThrottleProvider> next = throttleProvider.next();
            throttleProvider = next.getSecond();
            throttle = next.getFirst();
            if (throttle > 1.0 || throttle < 0.0) {
                throttleProvider.next();
                throttle = Math.max(0, Math.min(1, throttle));
            }
        }
        Move move = new Move();
        move.setThrottle(throttle);
        return move;
    }

    @Override
    protected void processCarCrash(Crash whoCrashed) {

    }

    @Override
    protected void processCarSpawn(Spawn whoSpawned) {

    }

    @Override
    public void finish() {
        super.finish();
        physics.logParams();
        try {
            PrintWriter writer = new PrintWriter("samples.dump");
            physics.dumpSamples(writer);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

interface ThrottleProvider {
    Pair<Double, ThrottleProvider> next();
}
