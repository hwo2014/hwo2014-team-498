package noobbot.scenarios;

import noobbot.BaseBot;
import noobbot.Track;
import noobbot.io.MessageSender;
import noobbot.logic.PhysicsStrategy;
import noobbot.logic.Strategy;
import noobbot.logic.World;
import noobbot.logic.stupidbots.ConstantThrottleBot;
import noobbot.model.Bot;
import noobbot.model.JoinRace;

import java.io.IOException;
import java.util.Random;

/**
 *   Runs a race with slow bots
 *
 *   Example of parameters:
 *     senna.helloworldopen.com 8091 "Team Saratov" 3Ad3PGsKxyVI5A
 */
public class RunAgainstSlowBots extends BaseBot {

    // select number of bots here
    private static final int NUMBER_OF_BOTS = 8;

    // select a track here
    private static final Track TRACK = Track.GERMANY;

    // password will be randomly generated
    private static final String PASSWORD = genRandomPassword();

    private static String genRandomPassword() {
        Random random = new Random();
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            buf.append((char) ('a' + random.nextInt(26)));
        }
        return buf.toString();
    }

    @SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
    private int id;

    public RunAgainstSlowBots(int id, String[] args) {
        this.id = id;
        this.args = args.clone();
        this.args[2] = getName(id);
    }

    public static void main(String... args) throws IOException, InterruptedException {
        Thread[] threads = new Thread[NUMBER_OF_BOTS];
        for (int i = 0; i < NUMBER_OF_BOTS; i++) {
            BaseBot bot = new RunAgainstSlowBots(i, args);
            threads[i] = new Thread(bot);
            threads[i].start();
            bot.waitUntilConnected();
        }
        for (int i = 0; i < NUMBER_OF_BOTS; i++) {
            threads[i].join();
        }
    }

    private double getThrottle(int id) {
        return 0.3 + 0.02 * (NUMBER_OF_BOTS - id);
    }

    private String getName(int id) {
        if (id == NUMBER_OF_BOTS - 1) {
            return args[2];
        } else {
            return "Slow " + String.format("%.2f", getThrottle(id));
        }
    }

    @Override
    public Strategy createStrategy(World world, MessageSender sender) {
        if (id == NUMBER_OF_BOTS - 1) {
            // last bot is always ours
            PhysicsStrategy strategy = new PhysicsStrategy(world, sender);
            strategy.enableCharts();
            return strategy;
        } else {
            // first bots are the slow ones
            ConstantThrottleBot bot = new ConstantThrottleBot(world, sender, getThrottle(id));
            bot.useShortestRacingLine();
            return bot;
        }
    }

    @Override
    protected void joinGame(Bot bot, MessageSender sender) {
        JoinRace join = new JoinRace(bot, NUMBER_OF_BOTS, TRACK.getName(), PASSWORD);
        sender.sendMessageNoTick(join);
    }

}