package noobbot.io;

import noobbot.model.MsgWrapper;
import java.io.BufferedReader;
import java.io.IOException;

public class MessageReader {

    private BufferedReader reader;

    public MessageReader(BufferedReader reader) {
        this.reader = reader;
    }

    public MsgWrapper readMessage() throws IOException {
        String line = reader.readLine();
        if (line == null) {
            return null;
        }
        // Log.error("[R] " + line);
        return MessageUtils.decodeJson(line, MsgWrapper.class);
    }

}
