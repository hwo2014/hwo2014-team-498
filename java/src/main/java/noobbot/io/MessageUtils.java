package noobbot.io;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import noobbot.model.MsgWrapper;
import noobbot.model.SwitchLane;
import noobbot.model.Throttle;
import noobbot.model.Turbo;

public class MessageUtils {

    private static final Gson GSON = new GsonBuilder()
            .registerTypeAdapter(MsgWrapper.class, new MsgWrapper.Adapter())
            .registerTypeAdapter(Throttle.class, new Throttle.Adapter())
            .registerTypeAdapter(SwitchLane.class, new SwitchLane.Adapter())
            .registerTypeAdapter(Turbo.class, new Turbo.Adapter())
            .create();

    public static <T> T decodeJson(String jsonData, Class<T> clazz) {
        return GSON.fromJson(jsonData, clazz);
    }
    public static <T> T decodeJson(JsonElement jsonData, Class<T> clazz) {
        return GSON.fromJson(jsonData, clazz);
    }

    public static <T> T decodeJson(JsonReader in, Class<T> clazz) {
        return GSON.fromJson(in, clazz);
    }

    public static String encodeJson(Object object) {
        return GSON.toJson(object);
    }

    public static TypeAdapter getAdapter(Class clazz) {
        return GSON.getAdapter(clazz);
    }

}
