package noobbot.io;

/**
 * Logger class
 */
public class Log {
    private static final ThreadLocal<Integer> currentTick = new ThreadLocal<>();

    synchronized public static void log(Object message) {
        System.out.println(String.format("[%s] [%d] %s", Thread.currentThread().getName(), getCurrentTick(), message));
        // System.out.flush();
    }

    synchronized public static void error(Object message) {
        System.err.println(String.format("[%s] [%d] %s", Thread.currentThread().getName(), getCurrentTick(), message));
        // System.err.flush();
    }

    synchronized public static void error(Object message, Throwable t) {
        System.err.println(String.format("[%s] [%d] %s %s", Thread.currentThread().getName(), getCurrentTick(), message, t.getMessage()));
        t.printStackTrace(System.err);
        // System.err.flush();
    }

    synchronized public static void important(Object message) {
        System.err.println(String.format("[%s] [%d] %s", Thread.currentThread().getName(), getCurrentTick(), message));
        // System.err.flush();
    }

    private static int getCurrentTick() {
        Integer tick = currentTick.get();
        return tick == null ? 0 : tick;
    }

    public static void setCurrentTick(int tick) {
        currentTick.set(tick);
    }
}
