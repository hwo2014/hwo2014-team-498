package noobbot.io;

import noobbot.model.MsgWrapper;

import java.io.PrintWriter;

public class MessageSender {

    private PrintWriter writer;
    private boolean messageSent;
    private long timeSpent;

    public MessageSender(PrintWriter writer) {
        this.writer = writer;
    }

    private void sendMessage(MsgWrapper msgWrapper) {
        long timeStart = System.currentTimeMillis();

        String msgJson = MessageUtils.encodeJson(msgWrapper);
        // Log.error("[W] " + msgJson);
        writer.println(msgJson);
        writer.flush();
        messageSent = true;

        long timeEnd = System.currentTimeMillis();
        timeSpent += timeEnd - timeStart;
    }

    public void sendMessageNoTick(Object object) {
        sendMessage(new MsgWrapper(object, null));
    }

    public void sendMessageWithTick(Object object, int gameTick) {
        sendMessage(new MsgWrapper(object, gameTick));
    }

    public void setMessageSent(boolean messageSent) {
        this.messageSent = messageSent;
        this.timeSpent = 0;
    }

    public boolean isMessageSent() {
        return messageSent;
    }

    public long getTimeSpent() {
        return timeSpent;
    }

}
